data "archive_file" "exercise_accomplishment_data" {
    type = "zip"
    output_path = "terraform_output/exercise_accomplishment_lambda_function_payload.zip"
    source_dir = "terraform_input/"
}

resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_fitledge_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "exercise_accomplishment_lambda_resource" {
  filename          = data.archive_file.exercise_accomplishment_data.output_path
  function_name     = "fitledge_exercise_accomplishment"
  description       = "This routine ensures a daily entry for each user_series_day combination on exercise_accomplishment table" 
  role              = aws_iam_role.iam_for_lambda.arn
  handler           = "entrypoint.handler"
  source_code_hash  = data.archive_file.exercise_accomplishment_data.output_base64sha256
  runtime           = "python3.7"
  memory_size       = 128
  timeout           = 60
  environment {
      variables = {
        rds_host=var.rds_host
        rds_username=var.rds_username
        rds_user_password=var.rds_user_password
        rds_db_name=var.rds_db_name
      }
  }
}

resource "aws_cloudwatch_event_rule" "exercise_accomplishment_event_rule" {
  name                = "fitledge_once_per_day_trigger"
  description         = "Will trigger once a day"
  schedule_expression = "cron(0 3 ? * * *)"
}

resource "aws_cloudwatch_event_target" "exercise_accomplishment_event_target" {
  rule  = aws_cloudwatch_event_rule.exercise_accomplishment_event_rule.name
  arn   = aws_lambda_function.exercise_accomplishment_lambda_resource.arn
}

resource "aws_lambda_permission" "exercise_accomplishment_allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.exercise_accomplishment_lambda_resource.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.exercise_accomplishment_event_rule.arn
}

variable "rds_host" {
    type = string
}
variable "rds_username" {
    type = string
}
variable "rds_user_password" {
    type = string
}
variable "rds_db_name" {
    type = string
}