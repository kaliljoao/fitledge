resource "aws_cognito_user_pool" "default_user_pool" {
  name = "fitledge_default_user"
  auto_verified_attributes = ["email"]
  verification_message_template {
    email_subject_by_link = "Fitledge Account Verification"
    email_message_by_link = "Hello,\n<br><br>\nThanks for signing up to Fitledge!!\n<br><br>\nPlease click the link to verify your email address: {##Click Here##}\n<br><br>"
    default_email_option = "CONFIRM_WITH_LINK"
  }

  password_policy {
    minimum_length = 6
    require_numbers = true
    require_uppercase = true
    require_lowercase = true
    require_symbols = false
  }

}

resource "aws_cognito_user_pool_client" "app_client" {
  name = "app_client"
  user_pool_id = aws_cognito_user_pool.default_user_pool.id
  refresh_token_validity = 3000  
}

resource "aws_cognito_user_pool_domain" "default_domain" {
  domain       = "fitledge"
  user_pool_id = aws_cognito_user_pool.default_user_pool.id
}



resource "local_file" "user_pool_id" {
    content = aws_cognito_user_pool.default_user_pool.id
    filename = "../assets/user_pool_id.txt"
}

resource "local_file" "client_id" {
    content = aws_cognito_user_pool_client.app_client.id
    filename = "../assets/client_id.txt"
}


