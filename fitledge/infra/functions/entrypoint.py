import sys
import logging
import json
import os
from datetime import datetime
import pytz

import psycopg2
# rds settings
rds_host = os.environ.get('rds_host')
rds_username = os.environ.get('rds_username')
rds_user_password = os.environ.get('rds_user_password')
rds_db_name = os.environ.get('rds_db_name')

logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn_string = "host=%s user=%s password=%s dbname=%s" % \
                    (rds_host, rds_username, rds_user_password, rds_db_name)
    conn = psycopg2.connect(conn_string)
except:
    logger.error("ERROR: Could not connect to Postgres instance.")
    sys.exit()

logger.info("SUCCESS: Connection to RDS Postgres instance succeeded")

def handler(event, context):
    utc = pytz.utc.localize(datetime.utcnow())
    now = utc.astimezone(pytz.timezone("Brazil/East"))

    query = f"""
    INSERT INTO user_series_accomplishment
    select u_s.username, u_s.series_id, FALSE, '{now.strftime("%Y-%m-%d")}'
    from user_series u_s
    where is_active is true
    ON CONFLICT DO NOTHING
    """

    with conn.cursor() as cur:
        cur.execute(query)
    conn.commit()
    conn.close()

    return { 'statusCode': 200 }