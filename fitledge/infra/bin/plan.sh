if [[ -d terraform_input/ ]]
    then rm -rf terraform_input/
fi
mkdir terraform_input/
cp -r functions/* terraform_input/
cp -r ../venv/lib/python3.7/site-packages/* terraform_input
##############
terraform plan -var-file="secret.tfvars"