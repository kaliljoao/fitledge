import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Widgets/Charts.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';
import 'package:charts_flutter/flutter.dart' as chart;
import 'package:postgres/postgres.dart';
part 'graphInfo.g.dart';

class GraphInfo = _GraphInfo with _$GraphInfo;

abstract class _GraphInfo with Store {
  @observable DateTime dataBeginDate = DateTime.now().subtract(Duration(days: 7));
  @observable DateTime dataEndDate = DateTime.now();
  void updateTimeIntervalDependantCharts() {
    this.generateSeriesCompletionData();
    this.generateTotalTimeSpentData();
    generateNumberOfDoneSeries();
  }

  // Graph's specifics:
  @observable bool isLoadingSeriesCompletionChartData = true;
  @observable List<chart.Series<SeriesCompletionData, DateTime>> seriesCompletionSeries = [];
  void generateSeriesCompletionData() async {
    this.isLoadingSeriesCompletionChartData = true;
    String query = """
      with all_count as (
        select count(done), date
        from user_series_accomplishment
        where username='${User.instance.username}' and
        date >= '${Graphs.dataBeginDate.year}-${Graphs.dataBeginDate.month}-${Graphs.dataBeginDate.day}' and
        date <= '${Graphs.dataEndDate.year}-${Graphs.dataEndDate.month}-${Graphs.dataEndDate.day}'
        group by date
      ), done_count as (
        select count(done), date
        from user_series_accomplishment
        where username='${User.instance.username}'and
        done is true and
        date >= '${Graphs.dataBeginDate.year}-${Graphs.dataBeginDate.month}-${Graphs.dataBeginDate.day}' and
        date <= '${Graphs.dataEndDate.year}-${Graphs.dataEndDate.month}-${Graphs.dataEndDate.day}'
        group by date
      )

      select distinct case when all_count."date" = done_count."date" then done_count.count
            else 0
          end / all_count.count::float as ratio, all_count.date as date
      from all_count, done_count
      where all_count."date" = done_count."date" or
        not exists (select 1 from done_count where "date" = all_count."date" )
      order by all_count."date" asc
    """;
    PostgreSQLConnection conn = await openConnection();
    List<List<dynamic>> res;
    try {
      res = await conn.query(query);
    }
    finally {
      conn.close();
    }
    List<SeriesCompletionData> seriesTypeDataCollection = [];
    for(List row in res) {
      double percentage = row[0];
      DateTime date = row[1];
      seriesTypeDataCollection.add(
        new SeriesCompletionData(
          date, 
          double.parse((percentage*100).toStringAsFixed(1)) // round to 1 decimal  
        ),
      );
    }

    List<chart.Series<SeriesCompletionData, DateTime>> seriesCompletionSeriesModel = [];
    seriesCompletionSeriesModel.add(
      chart.Series(
        id: "Weekly exercise completion",
        data: seriesTypeDataCollection,
        domainFn: (SeriesCompletionData seriesCompletionData, _) => seriesCompletionData.date,
        measureFn: (SeriesCompletionData seriesCompletionData, _) => seriesCompletionData.completionPercentage,
        colorFn: (__, _) => chart.ColorUtil.fromDartColor(Colors.deepOrange[900]),
      )
    );
    isLoadingSeriesCompletionChartData = false;
    this.seriesCompletionSeries = seriesCompletionSeriesModel;
  }


  @observable bool isLoadingTotalTimeCardData = true;
  @observable String totalTimeSpentData;
  void generateTotalTimeSpentData() async {
    this.isLoadingTotalTimeCardData = true;
    String query = """
    select sum(EXTRACT(epoch FROM (last_log."timestamp" - series_start."timestamp"))::int)
    from (
      select series, timestamp, timestamp::date as date
      from user_series_accomplishment_log
      where 
        timestamp::date >= '${Graphs.dataBeginDate.year}-${Graphs.dataBeginDate.month}-${Graphs.dataBeginDate.day}' and
        timestamp::date <= '${Graphs.dataEndDate.year}-${Graphs.dataEndDate.month}-${Graphs.dataEndDate.day}' and
        username = '${User.instance.username}' and
        status = 'series_start'
    ) as series_start, (
      select series, max(timestamp) as "timestamp", date
      from (
        select username, series, timestamp, timestamp::date as date, status
        from user_series_accomplishment_log
      ) as user_series_accomplishment_log_date
      where username = '${User.instance.username}'
      group by series, date
      having  
        date >= '${Graphs.dataBeginDate.year}-${Graphs.dataBeginDate.month}-${Graphs.dataBeginDate.day}' and
        date <= '${Graphs.dataEndDate.year}-${Graphs.dataEndDate.month}-${Graphs.dataEndDate.day}' and
        date= max(date)
    ) as last_log
    where series_start."date" = last_log."date" and
    series_start.series = last_log.series
    """;
    PostgreSQLConnection conn = await openConnection();
    var res;
    try {
      res = await conn.query(query);
    }
    finally {
      conn.close();
    }
    if(res[0].length == 0 || res[0][0] == null) totalTimeSpentData = "0h 0m";
    else{
      int total_minutes = res[0][0];
      int hours = (total_minutes / 3600).round();
      int minutes = ((total_minutes % 3600) / 60).round(); 
      if(hours > 0) {
        totalTimeSpentData = "${hours}h ${minutes}m";
      }
      else {
        totalTimeSpentData = "${minutes}m";
      }
    }
    this.isLoadingTotalTimeCardData = false;
  }

  @observable bool isLoadingNumberOfDoneSeries = true;
  @observable int numberOfDoneSeries;
  void generateNumberOfDoneSeries() async {
    this.isLoadingNumberOfDoneSeries = true;
    String query = """
    select count(series)
    from user_series_accomplishment
    where 
      date >= '${Graphs.dataBeginDate.year}-${Graphs.dataBeginDate.month}-${Graphs.dataBeginDate.day}' and
      date <= '${Graphs.dataEndDate.year}-${Graphs.dataEndDate.month}-${Graphs.dataEndDate.day}' and
      username = '${User.instance.username}' and
      done is true
    """;
    
    PostgreSQLConnection conn = await openConnection();
    var res;
    try {
      res = await conn.query(query);
    }
    finally {
      conn.close();
    }
    numberOfDoneSeries = res[0][0];
    isLoadingNumberOfDoneSeries = false;
  }
}