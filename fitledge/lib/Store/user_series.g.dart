// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_series.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserSeriesModel on _UserSeriesModel, Store {
  final _$testAtom = Atom(name: '_UserSeriesModel.test');

  @override
  int get test {
    _$testAtom.context.enforceReadPolicy(_$testAtom);
    _$testAtom.reportObserved();
    return super.test;
  }

  @override
  set test(int value) {
    _$testAtom.context.conditionallyRunInAction(() {
      super.test = value;
      _$testAtom.reportChanged();
    }, _$testAtom, name: '${_$testAtom.name}_set');
  }

  final _$_UserSeriesModelActionController =
      ActionController(name: '_UserSeriesModel');

  @override
  dynamic setTest(int test) {
    final _$actionInfo = _$_UserSeriesModelActionController.startAction();
    try {
      return super.setTest(test);
    } finally {
      _$_UserSeriesModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string = 'test: ${test.toString()}';
    return '{$string}';
  }
}
