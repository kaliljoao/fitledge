import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Assets/S3.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
part 'user.g.dart';

class UserModel = _UserModel with _$UserModel;

abstract class _UserModel with Store {
  // Database Attributes
  @observable String name;
  @observable String username;
  @observable double weight;
  @observable String gender;
  @observable String email;
  @observable int age;
  @observable double height;
  @observable int experience;

  // Internal Attributes
  @observable bool isLoadingUserData = true;
  @observable ImageProvider profileImage = AssetImage("assets/images/avatarImage.png");
  @observable String password;
  @observable CognitoUserSession userSession;

  Future<void> fetchUserData() async {
    return await new Future(() async {
      await retrieveUserInfo();
      profileImage = await getProfilePicture();
    });
  }

  @action setName(String name) {
    this.name = name;
  }

  @action setUsername(String username) {
    this.username = username;
  }

  @action setGender(String gender) {
    this.gender = gender;
  }

  @action setEmail(String email) {
    this.email = email;
  }

  @action setAge(int age) {
    this.age = age;
  }

  @action setHeight(double height) {
    this.height = height;
  }

  @action setWeight(double weight) {
    this.weight = weight;
  }
  
  @action setIsLoadingUserData(bool isLoadingUserData) {
    this.isLoadingUserData = isLoadingUserData;
  }

  @action setExperience(int experience) {
    this.experience = experience;
  }
}