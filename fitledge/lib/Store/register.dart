import 'package:mobx/mobx.dart';
part 'register.g.dart';

class RegisterData = _RegisterData with _$RegisterData;

abstract class _RegisterData with Store {
  @observable String username;
  @observable String password;
  @observable String email;
  @observable String name;
  @observable int age;
  @observable String gender;
  @observable double weight;
  @observable double height;
  @observable int experience;

  @action void setUsername(String username) {
    this.username = username;
  }

  @action void setPassword(String password) {
    this.password = password;
  }

  @action void setEmail(String email) {
    this.email = email;
  }

  @action void setName(String name) {
    this.name = name;
  }
  
  @action void setAge(int age) {
    this.age = age;
  }

  @action void setGender(String gender) {
    this.gender = gender;
  }

  @action void setWeight(double weight) {
    this.weight = weight;
  }

  @action void setHeight(double height) {
    this.height = height;
  }

  @action void setExperience(int experience) {
    this.experience = experience;
  }

  bool validateFields() {
    if(this.username == null ||
      this.password == null ||
      this.email == null ||
      this.name == null ||
      this.age == null ||
      this.gender == null ||
      this.weight == null ||
      this.height == null ||
      this.experience == null
    ) return false;
    return true;
  }
}