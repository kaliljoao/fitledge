// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'graphInfo.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GraphInfo on _GraphInfo, Store {
  final _$dataBeginDateAtom = Atom(name: '_GraphInfo.dataBeginDate');

  @override
  DateTime get dataBeginDate {
    _$dataBeginDateAtom.context.enforceReadPolicy(_$dataBeginDateAtom);
    _$dataBeginDateAtom.reportObserved();
    return super.dataBeginDate;
  }

  @override
  set dataBeginDate(DateTime value) {
    _$dataBeginDateAtom.context.conditionallyRunInAction(() {
      super.dataBeginDate = value;
      _$dataBeginDateAtom.reportChanged();
    }, _$dataBeginDateAtom, name: '${_$dataBeginDateAtom.name}_set');
  }

  final _$dataEndDateAtom = Atom(name: '_GraphInfo.dataEndDate');

  @override
  DateTime get dataEndDate {
    _$dataEndDateAtom.context.enforceReadPolicy(_$dataEndDateAtom);
    _$dataEndDateAtom.reportObserved();
    return super.dataEndDate;
  }

  @override
  set dataEndDate(DateTime value) {
    _$dataEndDateAtom.context.conditionallyRunInAction(() {
      super.dataEndDate = value;
      _$dataEndDateAtom.reportChanged();
    }, _$dataEndDateAtom, name: '${_$dataEndDateAtom.name}_set');
  }

  final _$isLoadingSeriesCompletionChartDataAtom =
      Atom(name: '_GraphInfo.isLoadingSeriesCompletionChartData');

  @override
  bool get isLoadingSeriesCompletionChartData {
    _$isLoadingSeriesCompletionChartDataAtom.context
        .enforceReadPolicy(_$isLoadingSeriesCompletionChartDataAtom);
    _$isLoadingSeriesCompletionChartDataAtom.reportObserved();
    return super.isLoadingSeriesCompletionChartData;
  }

  @override
  set isLoadingSeriesCompletionChartData(bool value) {
    _$isLoadingSeriesCompletionChartDataAtom.context.conditionallyRunInAction(
        () {
      super.isLoadingSeriesCompletionChartData = value;
      _$isLoadingSeriesCompletionChartDataAtom.reportChanged();
    }, _$isLoadingSeriesCompletionChartDataAtom,
        name: '${_$isLoadingSeriesCompletionChartDataAtom.name}_set');
  }

  final _$seriesCompletionSeriesAtom =
      Atom(name: '_GraphInfo.seriesCompletionSeries');

  @override
  List<chart.Series<SeriesCompletionData, DateTime>>
      get seriesCompletionSeries {
    _$seriesCompletionSeriesAtom.context
        .enforceReadPolicy(_$seriesCompletionSeriesAtom);
    _$seriesCompletionSeriesAtom.reportObserved();
    return super.seriesCompletionSeries;
  }

  @override
  set seriesCompletionSeries(
      List<chart.Series<SeriesCompletionData, DateTime>> value) {
    _$seriesCompletionSeriesAtom.context.conditionallyRunInAction(() {
      super.seriesCompletionSeries = value;
      _$seriesCompletionSeriesAtom.reportChanged();
    }, _$seriesCompletionSeriesAtom,
        name: '${_$seriesCompletionSeriesAtom.name}_set');
  }

  final _$isLoadingTotalTimeCardDataAtom =
      Atom(name: '_GraphInfo.isLoadingTotalTimeCardData');

  @override
  bool get isLoadingTotalTimeCardData {
    _$isLoadingTotalTimeCardDataAtom.context
        .enforceReadPolicy(_$isLoadingTotalTimeCardDataAtom);
    _$isLoadingTotalTimeCardDataAtom.reportObserved();
    return super.isLoadingTotalTimeCardData;
  }

  @override
  set isLoadingTotalTimeCardData(bool value) {
    _$isLoadingTotalTimeCardDataAtom.context.conditionallyRunInAction(() {
      super.isLoadingTotalTimeCardData = value;
      _$isLoadingTotalTimeCardDataAtom.reportChanged();
    }, _$isLoadingTotalTimeCardDataAtom,
        name: '${_$isLoadingTotalTimeCardDataAtom.name}_set');
  }

  final _$totalTimeSpentDataAtom = Atom(name: '_GraphInfo.totalTimeSpentData');

  @override
  String get totalTimeSpentData {
    _$totalTimeSpentDataAtom.context
        .enforceReadPolicy(_$totalTimeSpentDataAtom);
    _$totalTimeSpentDataAtom.reportObserved();
    return super.totalTimeSpentData;
  }

  @override
  set totalTimeSpentData(String value) {
    _$totalTimeSpentDataAtom.context.conditionallyRunInAction(() {
      super.totalTimeSpentData = value;
      _$totalTimeSpentDataAtom.reportChanged();
    }, _$totalTimeSpentDataAtom, name: '${_$totalTimeSpentDataAtom.name}_set');
  }

  final _$isLoadingNumberOfDoneSeriesAtom =
      Atom(name: '_GraphInfo.isLoadingNumberOfDoneSeries');

  @override
  bool get isLoadingNumberOfDoneSeries {
    _$isLoadingNumberOfDoneSeriesAtom.context
        .enforceReadPolicy(_$isLoadingNumberOfDoneSeriesAtom);
    _$isLoadingNumberOfDoneSeriesAtom.reportObserved();
    return super.isLoadingNumberOfDoneSeries;
  }

  @override
  set isLoadingNumberOfDoneSeries(bool value) {
    _$isLoadingNumberOfDoneSeriesAtom.context.conditionallyRunInAction(() {
      super.isLoadingNumberOfDoneSeries = value;
      _$isLoadingNumberOfDoneSeriesAtom.reportChanged();
    }, _$isLoadingNumberOfDoneSeriesAtom,
        name: '${_$isLoadingNumberOfDoneSeriesAtom.name}_set');
  }

  final _$numberOfDoneSeriesAtom = Atom(name: '_GraphInfo.numberOfDoneSeries');

  @override
  int get numberOfDoneSeries {
    _$numberOfDoneSeriesAtom.context
        .enforceReadPolicy(_$numberOfDoneSeriesAtom);
    _$numberOfDoneSeriesAtom.reportObserved();
    return super.numberOfDoneSeries;
  }

  @override
  set numberOfDoneSeries(int value) {
    _$numberOfDoneSeriesAtom.context.conditionallyRunInAction(() {
      super.numberOfDoneSeries = value;
      _$numberOfDoneSeriesAtom.reportChanged();
    }, _$numberOfDoneSeriesAtom, name: '${_$numberOfDoneSeriesAtom.name}_set');
  }

  @override
  String toString() {
    final string =
        'dataBeginDate: ${dataBeginDate.toString()},dataEndDate: ${dataEndDate.toString()},isLoadingSeriesCompletionChartData: ${isLoadingSeriesCompletionChartData.toString()},seriesCompletionSeries: ${seriesCompletionSeries.toString()},isLoadingTotalTimeCardData: ${isLoadingTotalTimeCardData.toString()},totalTimeSpentData: ${totalTimeSpentData.toString()},isLoadingNumberOfDoneSeries: ${isLoadingNumberOfDoneSeries.toString()},numberOfDoneSeries: ${numberOfDoneSeries.toString()}';
    return '{$string}';
  }
}
