import 'package:mobx/mobx.dart';
part 'user_series.g.dart';

class UserSeriesModel = _UserSeriesModel with _$UserSeriesModel;

abstract class _UserSeriesModel with Store {
  @observable int test;

  @action setTest(int test) {
    this.test = test;
  }
}