// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserModel on _UserModel, Store {
  final _$nameAtom = Atom(name: '_UserModel.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$usernameAtom = Atom(name: '_UserModel.username');

  @override
  String get username {
    _$usernameAtom.context.enforceReadPolicy(_$usernameAtom);
    _$usernameAtom.reportObserved();
    return super.username;
  }

  @override
  set username(String value) {
    _$usernameAtom.context.conditionallyRunInAction(() {
      super.username = value;
      _$usernameAtom.reportChanged();
    }, _$usernameAtom, name: '${_$usernameAtom.name}_set');
  }

  final _$weightAtom = Atom(name: '_UserModel.weight');

  @override
  double get weight {
    _$weightAtom.context.enforceReadPolicy(_$weightAtom);
    _$weightAtom.reportObserved();
    return super.weight;
  }

  @override
  set weight(double value) {
    _$weightAtom.context.conditionallyRunInAction(() {
      super.weight = value;
      _$weightAtom.reportChanged();
    }, _$weightAtom, name: '${_$weightAtom.name}_set');
  }

  final _$genderAtom = Atom(name: '_UserModel.gender');

  @override
  String get gender {
    _$genderAtom.context.enforceReadPolicy(_$genderAtom);
    _$genderAtom.reportObserved();
    return super.gender;
  }

  @override
  set gender(String value) {
    _$genderAtom.context.conditionallyRunInAction(() {
      super.gender = value;
      _$genderAtom.reportChanged();
    }, _$genderAtom, name: '${_$genderAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_UserModel.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$ageAtom = Atom(name: '_UserModel.age');

  @override
  int get age {
    _$ageAtom.context.enforceReadPolicy(_$ageAtom);
    _$ageAtom.reportObserved();
    return super.age;
  }

  @override
  set age(int value) {
    _$ageAtom.context.conditionallyRunInAction(() {
      super.age = value;
      _$ageAtom.reportChanged();
    }, _$ageAtom, name: '${_$ageAtom.name}_set');
  }

  final _$heightAtom = Atom(name: '_UserModel.height');

  @override
  double get height {
    _$heightAtom.context.enforceReadPolicy(_$heightAtom);
    _$heightAtom.reportObserved();
    return super.height;
  }

  @override
  set height(double value) {
    _$heightAtom.context.conditionallyRunInAction(() {
      super.height = value;
      _$heightAtom.reportChanged();
    }, _$heightAtom, name: '${_$heightAtom.name}_set');
  }

  final _$experienceAtom = Atom(name: '_UserModel.experience');

  @override
  int get experience {
    _$experienceAtom.context.enforceReadPolicy(_$experienceAtom);
    _$experienceAtom.reportObserved();
    return super.experience;
  }

  @override
  set experience(int value) {
    _$experienceAtom.context.conditionallyRunInAction(() {
      super.experience = value;
      _$experienceAtom.reportChanged();
    }, _$experienceAtom, name: '${_$experienceAtom.name}_set');
  }

  final _$isLoadingUserDataAtom = Atom(name: '_UserModel.isLoadingUserData');

  @override
  bool get isLoadingUserData {
    _$isLoadingUserDataAtom.context.enforceReadPolicy(_$isLoadingUserDataAtom);
    _$isLoadingUserDataAtom.reportObserved();
    return super.isLoadingUserData;
  }

  @override
  set isLoadingUserData(bool value) {
    _$isLoadingUserDataAtom.context.conditionallyRunInAction(() {
      super.isLoadingUserData = value;
      _$isLoadingUserDataAtom.reportChanged();
    }, _$isLoadingUserDataAtom, name: '${_$isLoadingUserDataAtom.name}_set');
  }

  final _$profileImageAtom = Atom(name: '_UserModel.profileImage');

  @override
  ImageProvider<dynamic> get profileImage {
    _$profileImageAtom.context.enforceReadPolicy(_$profileImageAtom);
    _$profileImageAtom.reportObserved();
    return super.profileImage;
  }

  @override
  set profileImage(ImageProvider<dynamic> value) {
    _$profileImageAtom.context.conditionallyRunInAction(() {
      super.profileImage = value;
      _$profileImageAtom.reportChanged();
    }, _$profileImageAtom, name: '${_$profileImageAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_UserModel.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$userSessionAtom = Atom(name: '_UserModel.userSession');

  @override
  CognitoUserSession get userSession {
    _$userSessionAtom.context.enforceReadPolicy(_$userSessionAtom);
    _$userSessionAtom.reportObserved();
    return super.userSession;
  }

  @override
  set userSession(CognitoUserSession value) {
    _$userSessionAtom.context.conditionallyRunInAction(() {
      super.userSession = value;
      _$userSessionAtom.reportChanged();
    }, _$userSessionAtom, name: '${_$userSessionAtom.name}_set');
  }

  final _$_UserModelActionController = ActionController(name: '_UserModel');

  @override
  dynamic setName(String name) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setName(name);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setUsername(String username) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setUsername(username);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setGender(String gender) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setGender(gender);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setEmail(String email) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setEmail(email);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setAge(int age) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setAge(age);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setHeight(double height) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setHeight(height);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setWeight(double weight) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setWeight(weight);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setIsLoadingUserData(bool isLoadingUserData) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setIsLoadingUserData(isLoadingUserData);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic setExperience(int experience) {
    final _$actionInfo = _$_UserModelActionController.startAction();
    try {
      return super.setExperience(experience);
    } finally {
      _$_UserModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'name: ${name.toString()},username: ${username.toString()},weight: ${weight.toString()},gender: ${gender.toString()},email: ${email.toString()},age: ${age.toString()},height: ${height.toString()},experience: ${experience.toString()},isLoadingUserData: ${isLoadingUserData.toString()},profileImage: ${profileImage.toString()},password: ${password.toString()},userSession: ${userSession.toString()}';
    return '{$string}';
  }
}
