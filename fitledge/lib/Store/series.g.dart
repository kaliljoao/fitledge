// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'series.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SeriesList on _SeriesList, Store {
  final _$listAtom = Atom(name: '_SeriesList.list');

  @override
  List<SeriesModel> get list {
    _$listAtom.context.enforceReadPolicy(_$listAtom);
    _$listAtom.reportObserved();
    return super.list;
  }

  @override
  set list(List<SeriesModel> value) {
    _$listAtom.context.conditionallyRunInAction(() {
      super.list = value;
      _$listAtom.reportChanged();
    }, _$listAtom, name: '${_$listAtom.name}_set');
  }

  final _$userListAtom = Atom(name: '_SeriesList.userList');

  @override
  List<SeriesModel> get userList {
    _$userListAtom.context.enforceReadPolicy(_$userListAtom);
    _$userListAtom.reportObserved();
    return super.userList;
  }

  @override
  set userList(List<SeriesModel> value) {
    _$userListAtom.context.conditionallyRunInAction(() {
      super.userList = value;
      _$userListAtom.reportChanged();
    }, _$userListAtom, name: '${_$userListAtom.name}_set');
  }

  final _$typeListAtom = Atom(name: '_SeriesList.typeList');

  @override
  List<String> get typeList {
    _$typeListAtom.context.enforceReadPolicy(_$typeListAtom);
    _$typeListAtom.reportObserved();
    return super.typeList;
  }

  @override
  set typeList(List<String> value) {
    _$typeListAtom.context.conditionallyRunInAction(() {
      super.typeList = value;
      _$typeListAtom.reportChanged();
    }, _$typeListAtom, name: '${_$typeListAtom.name}_set');
  }

  final _$defaultSeriesImagesAtom =
      Atom(name: '_SeriesList.defaultSeriesImages');

  @override
  Map<String, Image> get defaultSeriesImages {
    _$defaultSeriesImagesAtom.context
        .enforceReadPolicy(_$defaultSeriesImagesAtom);
    _$defaultSeriesImagesAtom.reportObserved();
    return super.defaultSeriesImages;
  }

  @override
  set defaultSeriesImages(Map<String, Image> value) {
    _$defaultSeriesImagesAtom.context.conditionallyRunInAction(() {
      super.defaultSeriesImages = value;
      _$defaultSeriesImagesAtom.reportChanged();
    }, _$defaultSeriesImagesAtom,
        name: '${_$defaultSeriesImagesAtom.name}_set');
  }

  final _$isLoadingSeriesAtom = Atom(name: '_SeriesList.isLoadingSeries');

  @override
  bool get isLoadingSeries {
    _$isLoadingSeriesAtom.context.enforceReadPolicy(_$isLoadingSeriesAtom);
    _$isLoadingSeriesAtom.reportObserved();
    return super.isLoadingSeries;
  }

  @override
  set isLoadingSeries(bool value) {
    _$isLoadingSeriesAtom.context.conditionallyRunInAction(() {
      super.isLoadingSeries = value;
      _$isLoadingSeriesAtom.reportChanged();
    }, _$isLoadingSeriesAtom, name: '${_$isLoadingSeriesAtom.name}_set');
  }

  final _$isLoadingUserSeriesAtom =
      Atom(name: '_SeriesList.isLoadingUserSeries');

  @override
  bool get isLoadingUserSeries {
    _$isLoadingUserSeriesAtom.context
        .enforceReadPolicy(_$isLoadingUserSeriesAtom);
    _$isLoadingUserSeriesAtom.reportObserved();
    return super.isLoadingUserSeries;
  }

  @override
  set isLoadingUserSeries(bool value) {
    _$isLoadingUserSeriesAtom.context.conditionallyRunInAction(() {
      super.isLoadingUserSeries = value;
      _$isLoadingUserSeriesAtom.reportChanged();
    }, _$isLoadingUserSeriesAtom,
        name: '${_$isLoadingUserSeriesAtom.name}_set');
  }

  final _$isLoadingDefaultSeriesImagesAtom =
      Atom(name: '_SeriesList.isLoadingDefaultSeriesImages');

  @override
  bool get isLoadingDefaultSeriesImages {
    _$isLoadingDefaultSeriesImagesAtom.context
        .enforceReadPolicy(_$isLoadingDefaultSeriesImagesAtom);
    _$isLoadingDefaultSeriesImagesAtom.reportObserved();
    return super.isLoadingDefaultSeriesImages;
  }

  @override
  set isLoadingDefaultSeriesImages(bool value) {
    _$isLoadingDefaultSeriesImagesAtom.context.conditionallyRunInAction(() {
      super.isLoadingDefaultSeriesImages = value;
      _$isLoadingDefaultSeriesImagesAtom.reportChanged();
    }, _$isLoadingDefaultSeriesImagesAtom,
        name: '${_$isLoadingDefaultSeriesImagesAtom.name}_set');
  }

  final _$_SeriesListActionController = ActionController(name: '_SeriesList');

  @override
  dynamic addSeries(SeriesModel series) {
    final _$actionInfo = _$_SeriesListActionController.startAction();
    try {
      return super.addSeries(series);
    } finally {
      _$_SeriesListActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addUserSeries(SeriesModel series) {
    final _$actionInfo = _$_SeriesListActionController.startAction();
    try {
      return super.addUserSeries(series);
    } finally {
      _$_SeriesListActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'list: ${list.toString()},userList: ${userList.toString()},typeList: ${typeList.toString()},defaultSeriesImages: ${defaultSeriesImages.toString()},isLoadingSeries: ${isLoadingSeries.toString()},isLoadingUserSeries: ${isLoadingUserSeries.toString()},isLoadingDefaultSeriesImages: ${isLoadingDefaultSeriesImages.toString()}';
    return '{$string}';
  }
}
