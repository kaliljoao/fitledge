import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
part 'exercises.g.dart';

class ExercisesList = _ExercisesList with _$ExercisesList;

abstract class _ExercisesList with Store {
  @observable List<ExerciseModel> list = [];
  @observable bool isLoadingExercises = true;
  @observable List<String> typeList = [];
  @action addExercise(ExerciseModel exercise) {
    this.list.add(exercise);
    if(!typeList.contains(exercise.type)) {
      typeList.add(exercise.type);
    }
  }
}

class ExerciseModel {
  String name;
  List<String> focus;
  String type;
  String description;
  String video_url;
  List<String> fotos_url;
  List<double> difficulty;
  String unit;
  double repetition_duration;
  double repetition_reverse_duration;              
  Image thumbnailImage;
  

  ExerciseModel (String name,
                String focus,
                String type,
                String description,
                String video_url,
                String fotos_url,
                String difficulty,
                String unit,
                double repetition_duration,
                double repetition_reverse_duration) {
    this.name = name;
    this.focus = focus.split(",");
    this.type = type;
    this.description = description;
    this.video_url = video_url;
    this.fotos_url = fotos_url.split(",");
    this.repetition_duration = repetition_duration;
    this.repetition_reverse_duration = repetition_reverse_duration;            
    if(difficulty != null){
      this.difficulty = new List<double>();
      List<String> splitList = difficulty.split(',');
      splitList.forEach((difficulty_value) {
        this.difficulty.add(double.parse(difficulty_value));
      });
    }
    this.unit = unit; 
  }

  void setThumbnailImage(Image thumbnailImage) {this.thumbnailImage = thumbnailImage;}
}
