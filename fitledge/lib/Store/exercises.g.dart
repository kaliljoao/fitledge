// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exercises.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ExercisesList on _ExercisesList, Store {
  final _$listAtom = Atom(name: '_ExercisesList.list');

  @override
  List<ExerciseModel> get list {
    _$listAtom.context.enforceReadPolicy(_$listAtom);
    _$listAtom.reportObserved();
    return super.list;
  }

  @override
  set list(List<ExerciseModel> value) {
    _$listAtom.context.conditionallyRunInAction(() {
      super.list = value;
      _$listAtom.reportChanged();
    }, _$listAtom, name: '${_$listAtom.name}_set');
  }

  final _$isLoadingExercisesAtom =
      Atom(name: '_ExercisesList.isLoadingExercises');

  @override
  bool get isLoadingExercises {
    _$isLoadingExercisesAtom.context
        .enforceReadPolicy(_$isLoadingExercisesAtom);
    _$isLoadingExercisesAtom.reportObserved();
    return super.isLoadingExercises;
  }

  @override
  set isLoadingExercises(bool value) {
    _$isLoadingExercisesAtom.context.conditionallyRunInAction(() {
      super.isLoadingExercises = value;
      _$isLoadingExercisesAtom.reportChanged();
    }, _$isLoadingExercisesAtom, name: '${_$isLoadingExercisesAtom.name}_set');
  }

  final _$typeListAtom = Atom(name: '_ExercisesList.typeList');

  @override
  List<String> get typeList {
    _$typeListAtom.context.enforceReadPolicy(_$typeListAtom);
    _$typeListAtom.reportObserved();
    return super.typeList;
  }

  @override
  set typeList(List<String> value) {
    _$typeListAtom.context.conditionallyRunInAction(() {
      super.typeList = value;
      _$typeListAtom.reportChanged();
    }, _$typeListAtom, name: '${_$typeListAtom.name}_set');
  }

  final _$_ExercisesListActionController =
      ActionController(name: '_ExercisesList');

  @override
  dynamic addExercise(ExerciseModel exercise) {
    final _$actionInfo = _$_ExercisesListActionController.startAction();
    try {
      return super.addExercise(exercise);
    } finally {
      _$_ExercisesListActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'list: ${list.toString()},isLoadingExercises: ${isLoadingExercises.toString()},typeList: ${typeList.toString()}';
    return '{$string}';
  }
}
