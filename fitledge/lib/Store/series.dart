import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
part 'series.g.dart';

class SeriesList = _SeriesList with _$SeriesList;

abstract class _SeriesList with Store {
  @observable List<SeriesModel> list = []; // Non-user made series
  @observable List<SeriesModel> userList = []; // All Series this user has added for himself
  @observable List<String> typeList = [];
  @observable Map<String, Image> defaultSeriesImages = {};
  @observable bool isLoadingSeries = true;
  @observable bool isLoadingUserSeries = true;
  @observable bool isLoadingDefaultSeriesImages = true;
  @action addSeries(SeriesModel series) {
    this.list.add(series);
  }
  @action addUserSeries(SeriesModel series) {
    this.userList.add(series);
  }
  removeUserSeries(SeriesModel series) {
    var userListCopy = Series.userList.where((SeriesModel _series) => _series != series).toList();
    this.userList = userListCopy; 
  }
}

class SeriesModel {
  @observable int id;
  @observable String name;
  @observable List<int> days = [];
  @observable List<String> daysStringFormat = []; // Week names format: Sunday, Monday ...
  @observable String duration;
  @observable bool isLoadingSeriesExercises = true;
  @observable List< Map<ExerciseModel,dynamic> > seriesExercises; // [ { ExerciseModel: {'repetitions':int, 'sets':int } }, ...] 

  SeriesModel(int id,String name,String duration, {String daysString, List<int> daysInt, List<Map<ExerciseModel,dynamic>> seriesExercises}) {
    this.id = id;
    this.name = name;
    if (daysInt != null) {
      this.days = daysInt;
      if(daysInt.contains(0)) this.daysStringFormat.add("Sunday");
      if(daysInt.contains(1)) this.daysStringFormat.add("Monday");
      if(daysInt.contains(2)) this.daysStringFormat.add("Tuesday");
      if(daysInt.contains(3)) this.daysStringFormat.add("Wednesday");
      if(daysInt.contains(4)) this.daysStringFormat.add("Thursday");
      if(daysInt.contains(5)) this.daysStringFormat.add("Friday");
      if(daysInt.contains(6)) this.daysStringFormat.add("Saturday");
    }
    else {
      daysString.split(',').map((day) {
        switch(day) {
          case '0':
            this.daysStringFormat.add("Sunday");
            break;
          case '1':
            this.daysStringFormat.add("Monday");
            break;
          case '2':
            this.daysStringFormat.add("Tuesday");
            break;
          case '3':
            this.daysStringFormat.add("Wednesday");
            break;
          case '4':
            this.daysStringFormat.add("Thursday");
            break;
          case '5':
            this.daysStringFormat.add("Friday");
            break;
          case '6':
            this.daysStringFormat.add("Saturday");
            break;
        }
        this.days.add(int.parse(day));
      }).toList();
    }
    this.duration = duration;
    if(seriesExercises != null) {
      this.seriesExercises = seriesExercises;
      this.isLoadingSeriesExercises = false;
    }
  }

  Future loadSeriesExercises() async {
    this.seriesExercises = await retrieveSeriesExercises(this.id);
    this.isLoadingSeriesExercises = false;
  }
}