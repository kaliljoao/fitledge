import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:mobx/mobx.dart';
import 'exercises.dart';
import 'series.dart';
part 'exerciseCompletion.g.dart';

class ExerciseCompletenessList = _ExerciseCompletenessList with _$ExerciseCompletenessList;

abstract class _ExerciseCompletenessList with Store {
  @observable List<ExerciseCompletenessModel>  execisesCompletenessList = [];
  @observable List<SeriesModel> completedSeries = [];
  @observable bool isLoadingExercisesCompleteness = true;
}

class ExerciseCompletenessModel {
  @observable DateTime timestamp;
  @observable SeriesModel series;
  @observable ExerciseModel exercise;
  @observable double completenessPercentage; // 80.5 == 80.5%
  @observable String status;

  ExerciseCompletenessModel({
    @required this.timestamp,
    @required this.series,
    @required this.exercise,
    @required this.completenessPercentage,
    @required this.status,
  });
}

bool statusExists({@required int seriesId, @required String exerciseName, @required String status, DateTime date}) {
  date ??= DateTime.now();
  if(status == "series_start" || status == "series_end") {
    var correspondingStatus = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
      final timeDiff = exerciseCompleteness.timestamp.difference(DateTime.now()).inDays;
      return exerciseCompleteness.series.id == seriesId &&
            (timeDiff == 0 && exerciseCompleteness.timestamp.day == DateTime.now().day) &&
            exerciseCompleteness.status == status;
    }).toList();
    return correspondingStatus.length > 0;
  }
  
  else if (status == "exercise_start" || status == "exercise_end"){
    var correspondingStatus = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
      final timeDiff = exerciseCompleteness.timestamp.difference(DateTime.now()).inDays;
      return exerciseCompleteness.series.id == seriesId && 
            exerciseCompleteness.exercise != null &&
            exerciseCompleteness.exercise.name == exerciseName &&
            (timeDiff == 0 && exerciseCompleteness.timestamp.day == DateTime.now().day) &&
            exerciseCompleteness.status == status;
    }).toList();
    return correspondingStatus.length > 0;
  }

  else if (status == "exercise_completeness_") {
    var correspondingStatus = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
      final timeDiff = exerciseCompleteness.timestamp.difference(DateTime.now()).inDays;
      return exerciseCompleteness.series.id == seriesId && 
            exerciseCompleteness.exercise != null &&
            exerciseCompleteness.exercise.name == exerciseName &&
            (timeDiff == 0 && exerciseCompleteness.timestamp.day == DateTime.now().day) &&
            exerciseCompleteness.status.startsWith(status);
    }).toList();
    return correspondingStatus.length > 0;
  }
}

double getExerciseCompleteness({@required SeriesModel series, @required ExerciseModel exercise}) {
  if(!statusExists(seriesId: series.id, exerciseName: exercise.name, status: "exercise_completeness_")) return 0.0;
  List<ExerciseCompletenessModel> exercisesCompletenessPercentage = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
    final timeDiff = exerciseCompleteness.timestamp.difference(DateTime.now()).inDays;
    return exerciseCompleteness.series == series &&
          exerciseCompleteness.exercise == exercise &&
          exerciseCompleteness.completenessPercentage != null &&
          (timeDiff == 0 && exerciseCompleteness.timestamp.day == DateTime.now().day);
  }).toList();
  double _max = 0;
  for(ExerciseCompletenessModel exerciseCompleteness in exercisesCompletenessPercentage) {
    double percentage = exerciseCompleteness.completenessPercentage;
    if(percentage > _max) {
      _max = percentage;
    }
  }
  print(_max);
  return _max / 100;
}

bool checkSeriesFinished({@required SeriesModel series}) {
  List<ExerciseModel> seriesExercises = [];
  for(Map<ExerciseModel, dynamic> exerciseMap in series.seriesExercises) {
    seriesExercises.add(exerciseMap.keys.toList()[0]);
  }
  List completedSeriesExercises = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
    final timeDiff = exerciseCompleteness.timestamp.difference(DateTime.now()).inDays;
    return exerciseCompleteness.series == series &&
    (timeDiff == 0 && exerciseCompleteness.timestamp.day == DateTime.now().day) &&
    exerciseCompleteness.status == "exercise_end";
  }).toList(); 

  return completedSeriesExercises.length == series.seriesExercises.length;
}