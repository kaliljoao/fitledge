// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$RegisterData on _RegisterData, Store {
  final _$usernameAtom = Atom(name: '_RegisterData.username');

  @override
  String get username {
    _$usernameAtom.context.enforceReadPolicy(_$usernameAtom);
    _$usernameAtom.reportObserved();
    return super.username;
  }

  @override
  set username(String value) {
    _$usernameAtom.context.conditionallyRunInAction(() {
      super.username = value;
      _$usernameAtom.reportChanged();
    }, _$usernameAtom, name: '${_$usernameAtom.name}_set');
  }

  final _$passwordAtom = Atom(name: '_RegisterData.password');

  @override
  String get password {
    _$passwordAtom.context.enforceReadPolicy(_$passwordAtom);
    _$passwordAtom.reportObserved();
    return super.password;
  }

  @override
  set password(String value) {
    _$passwordAtom.context.conditionallyRunInAction(() {
      super.password = value;
      _$passwordAtom.reportChanged();
    }, _$passwordAtom, name: '${_$passwordAtom.name}_set');
  }

  final _$emailAtom = Atom(name: '_RegisterData.email');

  @override
  String get email {
    _$emailAtom.context.enforceReadPolicy(_$emailAtom);
    _$emailAtom.reportObserved();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.context.conditionallyRunInAction(() {
      super.email = value;
      _$emailAtom.reportChanged();
    }, _$emailAtom, name: '${_$emailAtom.name}_set');
  }

  final _$nameAtom = Atom(name: '_RegisterData.name');

  @override
  String get name {
    _$nameAtom.context.enforceReadPolicy(_$nameAtom);
    _$nameAtom.reportObserved();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.context.conditionallyRunInAction(() {
      super.name = value;
      _$nameAtom.reportChanged();
    }, _$nameAtom, name: '${_$nameAtom.name}_set');
  }

  final _$ageAtom = Atom(name: '_RegisterData.age');

  @override
  int get age {
    _$ageAtom.context.enforceReadPolicy(_$ageAtom);
    _$ageAtom.reportObserved();
    return super.age;
  }

  @override
  set age(int value) {
    _$ageAtom.context.conditionallyRunInAction(() {
      super.age = value;
      _$ageAtom.reportChanged();
    }, _$ageAtom, name: '${_$ageAtom.name}_set');
  }

  final _$genderAtom = Atom(name: '_RegisterData.gender');

  @override
  String get gender {
    _$genderAtom.context.enforceReadPolicy(_$genderAtom);
    _$genderAtom.reportObserved();
    return super.gender;
  }

  @override
  set gender(String value) {
    _$genderAtom.context.conditionallyRunInAction(() {
      super.gender = value;
      _$genderAtom.reportChanged();
    }, _$genderAtom, name: '${_$genderAtom.name}_set');
  }

  final _$weightAtom = Atom(name: '_RegisterData.weight');

  @override
  double get weight {
    _$weightAtom.context.enforceReadPolicy(_$weightAtom);
    _$weightAtom.reportObserved();
    return super.weight;
  }

  @override
  set weight(double value) {
    _$weightAtom.context.conditionallyRunInAction(() {
      super.weight = value;
      _$weightAtom.reportChanged();
    }, _$weightAtom, name: '${_$weightAtom.name}_set');
  }

  final _$heightAtom = Atom(name: '_RegisterData.height');

  @override
  double get height {
    _$heightAtom.context.enforceReadPolicy(_$heightAtom);
    _$heightAtom.reportObserved();
    return super.height;
  }

  @override
  set height(double value) {
    _$heightAtom.context.conditionallyRunInAction(() {
      super.height = value;
      _$heightAtom.reportChanged();
    }, _$heightAtom, name: '${_$heightAtom.name}_set');
  }

  final _$experienceAtom = Atom(name: '_RegisterData.experience');

  @override
  int get experience {
    _$experienceAtom.context.enforceReadPolicy(_$experienceAtom);
    _$experienceAtom.reportObserved();
    return super.experience;
  }

  @override
  set experience(int value) {
    _$experienceAtom.context.conditionallyRunInAction(() {
      super.experience = value;
      _$experienceAtom.reportChanged();
    }, _$experienceAtom, name: '${_$experienceAtom.name}_set');
  }

  final _$_RegisterDataActionController =
      ActionController(name: '_RegisterData');

  @override
  void setUsername(String username) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setUsername(username);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPassword(String password) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setPassword(password);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEmail(String email) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setEmail(email);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setName(String name) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setName(name);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setAge(int age) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setAge(age);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setGender(String gender) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setGender(gender);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setWeight(double weight) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setWeight(weight);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setHeight(double height) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setHeight(height);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setExperience(int experience) {
    final _$actionInfo = _$_RegisterDataActionController.startAction();
    try {
      return super.setExperience(experience);
    } finally {
      _$_RegisterDataActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    final string =
        'username: ${username.toString()},password: ${password.toString()},email: ${email.toString()},name: ${name.toString()},age: ${age.toString()},gender: ${gender.toString()},weight: ${weight.toString()},height: ${height.toString()},experience: ${experience.toString()}';
    return '{$string}';
  }
}
