// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exerciseCompletion.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ExerciseCompletenessList on _ExerciseCompletenessList, Store {
  final _$execisesCompletenessListAtom =
      Atom(name: '_ExerciseCompletenessList.execisesCompletenessList');

  @override
  List<ExerciseCompletenessModel> get execisesCompletenessList {
    _$execisesCompletenessListAtom.context
        .enforceReadPolicy(_$execisesCompletenessListAtom);
    _$execisesCompletenessListAtom.reportObserved();
    return super.execisesCompletenessList;
  }

  @override
  set execisesCompletenessList(List<ExerciseCompletenessModel> value) {
    _$execisesCompletenessListAtom.context.conditionallyRunInAction(() {
      super.execisesCompletenessList = value;
      _$execisesCompletenessListAtom.reportChanged();
    }, _$execisesCompletenessListAtom,
        name: '${_$execisesCompletenessListAtom.name}_set');
  }

  final _$completedSeriesAtom =
      Atom(name: '_ExerciseCompletenessList.completedSeries');

  @override
  List<SeriesModel> get completedSeries {
    _$completedSeriesAtom.context.enforceReadPolicy(_$completedSeriesAtom);
    _$completedSeriesAtom.reportObserved();
    return super.completedSeries;
  }

  @override
  set completedSeries(List<SeriesModel> value) {
    _$completedSeriesAtom.context.conditionallyRunInAction(() {
      super.completedSeries = value;
      _$completedSeriesAtom.reportChanged();
    }, _$completedSeriesAtom, name: '${_$completedSeriesAtom.name}_set');
  }

  final _$isLoadingExercisesCompletenessAtom =
      Atom(name: '_ExerciseCompletenessList.isLoadingExercisesCompleteness');

  @override
  bool get isLoadingExercisesCompleteness {
    _$isLoadingExercisesCompletenessAtom.context
        .enforceReadPolicy(_$isLoadingExercisesCompletenessAtom);
    _$isLoadingExercisesCompletenessAtom.reportObserved();
    return super.isLoadingExercisesCompleteness;
  }

  @override
  set isLoadingExercisesCompleteness(bool value) {
    _$isLoadingExercisesCompletenessAtom.context.conditionallyRunInAction(() {
      super.isLoadingExercisesCompleteness = value;
      _$isLoadingExercisesCompletenessAtom.reportChanged();
    }, _$isLoadingExercisesCompletenessAtom,
        name: '${_$isLoadingExercisesCompletenessAtom.name}_set');
  }

  @override
  String toString() {
    final string =
        'execisesCompletenessList: ${execisesCompletenessList.toString()},completedSeries: ${completedSeries.toString()},isLoadingExercisesCompleteness: ${isLoadingExercisesCompleteness.toString()}';
    return '{$string}';
  }
}
