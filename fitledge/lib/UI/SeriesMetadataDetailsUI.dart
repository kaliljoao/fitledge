import 'package:fitledge/Assets/DayPicker.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/UI/SeriesUserUI.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';

import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Store/series.dart';

class SeriesUserDetailsUI extends StatefulWidget{
  List<Map<ExerciseModel, Map<String, int>>> listExercises;
  SeriesModel series;

  SeriesUserDetailsUI({Key key, this.listExercises, this.series}) : super(key: key);

  @override
  _SeriesUserDetailsUIState createState() => _SeriesUserDetailsUIState(this.listExercises, 
                                                                        this.series);
}

class _SeriesUserDetailsUIState extends State<SeriesUserDetailsUI> {
  List<Map<ExerciseModel, Map<String, int>>> listExercises;
  SeriesModel series;
  TextEditingController nameSerieController;
  // TextEditingController daysSerieController;
  List<int> chosenDays;
  TextEditingController durationSerieController;
  bool isCreatingSeries = false;

  _SeriesUserDetailsUIState(List<Map<ExerciseModel, Map<String, int>>> _listExercises, SeriesModel _series) {
    this.listExercises = _listExercises;
    this.series = _series;
  }

  void initState() {
    nameSerieController = new TextEditingController(text: this.series != null ? this.series.name : "");
    // daysSerieController = new TextEditingController(text: this.series != null ? this.series.days.toString() : "");
    chosenDays = this.series != null ? this.series.days : [];
    durationSerieController = new TextEditingController(text: this.series != null ? this.series.duration : "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text(Localization.of(context).translate("MetadataSeries_Title")),
        centerTitle: true,
        backgroundColor: Colors.black87,
        elevation: 1,
      ),
      body: Container(
        color: Colors.black87,
        padding: EdgeInsets.only(right: 15, left: 15, top: 40),
        child: ListView(
          children: <Widget>[
            TextFormField(
              style: TextStyle(color: Colors.white),
              controller: nameSerieController,
              cursorColor: Colors.deepOrange,
              decoration: InputDecoration(
                prefixIcon: Icon(
                  Ionicons.ios_fitness,
                  color: Colors.deepOrange,
                ),
                labelText: Localization.of(context).translate("MetadataSeries_Name"),
                labelStyle: TextStyle(
                  color: Colors.white
                ),
                fillColor: Colors.black87,
                hoverColor: Colors.black87,
                focusColor: Colors.black87
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 12.0, right: 12.0),
                  child: Icon(
                    Icons.calendar_today,
                    color: Colors.deepOrange,
                  ),
                ),
                Text(
                  Localization.of(context).translate("MetadataSeries_Days"),
                  style: TextStyle(color: Colors.white, fontSize: 16),
                )
              ],
            ),
            SizedBox(
              height: 12,
            ),
            Theme(
              data: Theme.of(context).copyWith(
                accentColor: Colors.black54,
                textTheme: TextTheme(
                  button: TextStyle(
                    color: Colors.black87
                  )
                )
              ),
              child: MyDaySelector(
                value: chosenDays,
                onChange: (value) {
                  this.chosenDays = value;
                },
                mode: MyDaySelector.modeFull
              ),
            ),
            SizedBox(
              height: 6,
            ),
            Divider(
              color: Colors.black,
            ),
            // TextFormField(
            //   style: TextStyle(color: Colors.white),
            //   controller: durationSerieController,
            //   cursorColor: Colors.deepOrange,
            //   decoration: InputDecoration(
            //     prefixIcon: Icon(
            //       Icons.alarm,
            //       color: Colors.deepOrange,
            //     ),
            //     labelText: 'Duration',
            //     labelStyle: TextStyle(
            //       color: Colors.white
            //     ),
            //     fillColor: Colors.black87,
            //     hoverColor: Colors.black87,
            //     focusColor: Colors.black87
            //   ),
            // ),
            SizedBox(
              height: 150,
            ),
                  // Icon(
                  //   Icons.calendar_today,
                  //   color: Colors.deepOrange,
                  // ),
            SizedBox(
              height: 25,
            ),
            GestureDetector(
              onTap: () async { 
                if(!isCreatingSeries) {
                  setState(() => isCreatingSeries = true);
                  // SeriesModel newSerie = new SeriesModel(0, nameSerieController.text, durationSerieController.text, daysInt: chosenDays);
                  SeriesModel newSerie = new SeriesModel(-1, nameSerieController.text, "01:00:00", daysInt: chosenDays, seriesExercises: []);
                  if(listExercises != null) /* Custom user series, with chosen exercise list */ {
                    newSerie.seriesExercises = listExercises;
                    newSerie.id = await insertUserCreatedSeries(newSerie, listExercises);
                  }
                  else if( newSerie.name != this.series.name ||
                    newSerie.days != this.series.days ||
                    newSerie.duration != this.series.duration) /* Created from default series but changed some of the values */ {
                      newSerie.seriesExercises = this.series.seriesExercises;
                      newSerie.id = await insertUserCreatedSeries(newSerie, this.series.seriesExercises);
                  }
                  else {
                    newSerie.id = this.series.id;
                    newSerie.seriesExercises = this.series.seriesExercises;
                    await insertUserSeries(newSerie);
                  }
                  setState(() => isCreatingSeries = false);
                  Series.appendUserSeries(newSerie);
                  if(listExercises != null){
                    Navigator.popUntil(context, 
                      (route) {
                        return route.settings.name == "/userSeries";
                        // recarregar UI após isso
                      }
                    );   
                  }
                  else {
                    Navigator.popUntil(context, 
                      (route) {
                        return route.settings.name == "/home";
                      }
                    );
                  }
                }
              },
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 65,
                child: Container(
                  color: Colors.black38,
                  child: Center(
                    child: !isCreatingSeries ? Text(
                      Localization.of(context).translate("MetadataSeries_Confirm"),
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.deepOrange
                      ),
                    ) :
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation(Colors.deepOrange),
                    )
                  ),
                ),
              )
            ),
          ],
        ),
      ),
    );
  }

}