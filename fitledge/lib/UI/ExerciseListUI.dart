import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Widgets/SeriesListView.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'SeriesDetailsUI.dart';

class ExerciseListUI extends StatefulWidget {
  State<ExerciseListUI> createState() => _ExerciseListUIState();
}

class _ExerciseListUIState extends State<ExerciseListUI> {
  Widget _getListViewItems() {
    if(Series.isLoadingUserSeries || ExercisesCompleteness.isLoadingExercisesCompleteness) {
      return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.deepOrange),
              )
            ])
          ],
        )
      );
    }

    TextTheme textTheme = Theme.of(context).textTheme;
    List<Widget> list = <Widget>[
      SizedBox(height: 10,),
      Container(
        padding: EdgeInsets.symmetric(horizontal: 15.0),
        child: Text(
          Localization.of(context).translate("Exercise_Title"),
          style: textTheme.headline.copyWith(color:Colors.white, fontWeight: FontWeight.w500),
        ),
      ),
      SizedBox(height: 10,),
      Expanded(
        child: SeriesListView(
          context,
          _getTodaysSeries(), 
          true,
          onCardPressed: (series) {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SeriesDetailUI(series, true)));
          }
        ),
      )
    ];
    return Column(children: list,);
  }

  List<SeriesModel> _getTodaysSeries() {
    List<SeriesModel> list = [];
    var today = DateTime.now().toLocal().weekday % 7;
    for(SeriesModel series in Series.userList) {
      if(series.days.contains(today)) {
        list.add(series);
      }
    }
    return list;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          Localization.of(context).translate("Exercise_Exercise"),
        ),
        centerTitle: true,
        backgroundColor: Colors.black87,
        elevation: 1,
      ),
      body: Container(
        color: Colors.black87,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Observer(
          builder: (_) => _getListViewItems()
        ),
      ) 
    );
  }
}