import 'package:fitledge/Assets/Cognito.dart';
import 'package:fitledge/Assets/Dialogs.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Assets/Postgres.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitledge/Store/register.dart';
import 'package:flutter/services.dart';

final RegisterData registerData = RegisterData();

class RegisterUI_1 extends StatefulWidget {
  RegisterUI_1({Key key,}) : super(key: key);
  
  @override
  _RegisterUIState_1 createState() => _RegisterUIState_1();
}

class _RegisterUIState_1 extends State<RegisterUI_1> {
  TextEditingController _emailController;
  TextEditingController _usernameController;
  TextEditingController _passwordController;
  bool _isLoading;
  @override
  void initState() {
    _emailController = new TextEditingController();
    _usernameController = new TextEditingController();
    _passwordController = new TextEditingController();
    _isLoading = false;
  }

  void _redirectToHome() {
    Navigator.pushNamedAndRemoveUntil(context, "/home", (_) => false);
  }

  void _handleContinueTap() async {
    setState(() {
      _isLoading = true;
    });
    bool fieldsValid = await _verifyFields();
    setState(() {
      _isLoading = false;
    });
    if(fieldsValid) {
      Navigator.pushNamed(context, "/register/2");
    }
  }

  Widget _getContinueButton() {
    if (!_isLoading) {
      return Text(
        Localization.of(context).translate("Register_btnContinue"),
        style: TextStyle(fontSize: 20, color: Colors.deepOrange),
      ); 
    }
    else {
      return Container(
        width: 24,
        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.deepOrange)),
      );
    }
  }

  Future<bool> _verifyFields() async {
    return await new Future (() async {
      if(_usernameController.text == "" || 
              _passwordController.text == "" ||
              _emailController.text == "") {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return ErrorMessageDialog(Localization.of(context).translate("Register_Message_emptyField"));
          }
        );
        return false;
      }
      else {
        bool userExists = await checkUserExists(_usernameController.text);
        if(userExists) {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return ErrorMessageDialog(Localization.of(context).translate("Register_Message_existsUsername"));
            }
          );
          return false;
        }
        return true;
      }
    });
  }

  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.all(20),
      color: Colors.black87,
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: 0,
          ),
          Center(
            child: Image.asset("assets/images/logo.png")
          ),
          SizedBox(
            height: 0,
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25)
            ),
            margin: EdgeInsets.all(20.0),
            child: Container(
              padding: EdgeInsets.only(left: 25, right: 25, top: 25, bottom: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    onChanged: (string) {
                      registerData.setEmail(string);
                    },
                    style: TextStyle (
                      color: Colors.black87
                    ),
                    cursorColor: Colors.black87,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.email),
                      labelText: 'Email',
                      labelStyle: TextStyle(
                        color: Colors.black87
                      ),
                      fillColor: Colors.black87,
                      hoverColor: Colors.black87,
                      focusColor: Colors.black87,
                      
                    ),
                    controller: _emailController,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    onChanged: (string) {
                      registerData.setUsername(string);
                    },
                    style: TextStyle (
                      color: Colors.black87
                    ),
                    cursorColor: Colors.black87,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.people),
                      labelText: Localization.of(context).translate("Register_Username"),
                      labelStyle: TextStyle(
                        color: Colors.black87
                      ),
                      fillColor: Colors.black87,
                      hoverColor: Colors.black87,
                      focusColor: Colors.black87
                    ),
                    controller: _usernameController,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    onChanged: (string) {
                      registerData.setPassword(string);
                    },
                    style: TextStyle (
                      color: Colors.black87
                    ),
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.vpn_key),
                      labelText: Localization.of(context).translate("Register_Password"),
                      labelStyle: TextStyle(
                        color: Colors.black87
                      ),
                      fillColor: Colors.black87,
                      hoverColor: Colors.black87,
                      focusColor: Colors.black87
                    ),
                    controller: _passwordController,
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  InkWell(
                    onTap: () async {
                      _handleContinueTap();
                    },
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 13),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.black87,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[850],
                            blurRadius: 5.0, // has the effect of softening the shadow
                            spreadRadius: 1.0, // has the effect of extending the shadow
                            offset: Offset(
                              0.0, // horizontal, move right 10
                              5.0, // vertical, move down 10
                            ),
                          )
                        ],
                      ),
                      child: _getContinueButton(),
                    ),
                  ),
                  SizedBox(
                  height: 20,
                  ),
                  
                ],
              ),
            )
          ),
        ],
      ),
    );
  }
}

class RegisterUI_2 extends StatefulWidget {
  RegisterUI_2({Key key,}) : super(key: key);
  
  @override
  _RegisterUIState_2 createState() => _RegisterUIState_2();
}

class _RegisterUIState_2 extends State<RegisterUI_2> {
  TextEditingController _nameController;
  TextEditingController _ageController;
  TextEditingController _weightController;
  TextEditingController _heightController;
  String _gender = registerData.gender;
  final List<String> _experience_values = ["Novice", "Amateur", "Intermediate", "Experienced"];
  String _experience;
  bool _isLoading;
  @override
  void initState() {
    _nameController = new TextEditingController(text:registerData.name);
    if(registerData.age != null) _ageController = new TextEditingController(
      text:registerData.age.toString()
    );
    if(registerData.weight != null) _weightController = new TextEditingController(
      text:registerData.weight.toString()
    );
    if(registerData.height != null) _heightController = new TextEditingController(
      text:registerData.height.toString()
    );
    _isLoading = false;
    if(registerData.experience != null) _experience = _experience_values[registerData.experience];
  }

  void _redirectToHome() {
    Navigator.pushNamedAndRemoveUntil(context, "/home", (_) => false);
  }

  void _handleRegisterTap()  {
    setState(() {
      _isLoading = true;
    });
    bool fieldsAreValid = _validateFields();
    if(fieldsAreValid) {
      registerUser(
        registerData
      ).then((_) async {  
        insertUser(registerData); // Insert user data into Postgres DB
        setState(() {
          _isLoading = false;
        });
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return WaitingConfirmationDialog(registerData.username);
          }
        );
        await waitConfirmation(
          registerData.username,
          registerData.password
        );
        _redirectToHome();
      }).catchError((exc) {
        setState(() {
          _isLoading = false;
        });
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AuthenticationErrorDialog(exc);
          }
        );
      });
    }
    else {
      setState(() {
        _isLoading = false;
      });
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return ErrorMessageDialog(Localization.of(context).translate("Register_Message_emptyField"));
        }
      );
    }
  }

  Widget _getRegisterButton() {
    if (!_isLoading) {
      return Text(
        Localization.of(context).translate("Register_Register"),
        style: TextStyle(fontSize: 20, color: Colors.deepOrange),
      ); 
    }
    else {
      return Container(
        width: 24,
        child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.deepOrange)),
      );
    }
  }

  bool _validateFields() {
    return registerData.validateFields();
  }


  Widget build(BuildContext context) {
    
    _experience_values[0] = Localization.of(context).translate("ExperienceValues_Novice");
    _experience_values[1] = Localization.of(context).translate("ExperienceValues_Amateur");
    _experience_values[2] = Localization.of(context).translate("ExperienceValues_Intermediate");
    _experience_values[3] = Localization.of(context).translate("ExperienceValues_Experienced");

    return new Container(
      padding: EdgeInsets.symmetric(vertical:10.0, horizontal:20.0),
      color: Colors.black87,
      child: ListView(
        children: <Widget>[
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25)
            ),
            margin: EdgeInsets.symmetric(horizontal:20.0, vertical: 0.0),
            child: Container(
              padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 5),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    TextFormField(
                      onChanged: (string) {registerData.setName(string);},
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      cursorColor: Colors.black87,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.accessibility_new),
                        labelText: Localization.of(context).translate("Register_Fullname"),
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87,
                      ),
                      controller: _nameController,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    DropdownButtonFormField(
                      value: _gender,
                      items: [Localization.of(context).translate("Gender_Male"), Localization.of(context).translate("Gender_Female"), Localization.of(context).translate("Gender_Other")]
                      .map((label) => DropdownMenuItem(
                        child: Text(label),
                        value: label,
                        key: Key(label),
                      )).toList(),
                      onChanged: (string) {
                        registerData.setGender(string);
                        setState(() {
                          _gender = string;  
                        });
                      },
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(0.0),
                        prefixIcon: Icon(
                          Icons.wc,
                          size: 24.0,
                        ),
                        labelText: Localization.of(context).translate("Register_Gender"),
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87
                      ),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                      ],
                      onChanged: (string) {registerData.setAge(int.parse(string));},
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.cake),
                        labelText: Localization.of(context).translate("Register_Age"),
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87
                      ),
                      controller: _ageController,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.numberWithOptions(decimal:true),
                      inputFormatters: [
                          NumberTextInputFormatter(decimalRange: 2)
                      ],
                      onChanged: (string) {registerData.setWeight(double.parse(string));},
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.restaurant_menu),
                        labelText: Localization.of(context).translate("Register_Weight"),
                        suffixText: '(kg)',
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87
                      ),
                      controller: _weightController,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.numberWithOptions(decimal:true),
                      inputFormatters: [
                          NumberTextInputFormatter(decimalRange: 2)
                      ],
                      onChanged: (string) {registerData.setHeight(double.parse(string));},
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.straighten),
                        labelText: Localization.of(context).translate("Register_Height"),
                        suffixText: '(m)',
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87
                      ),
                      controller: _heightController,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    DropdownButtonFormField(
                      value: _experience,
                      items: _experience_values.asMap()
                      .map((index,label) => MapEntry(index,  
                        DropdownMenuItem(
                          child: Text(label),
                          value: label,
                          key: Key(label),
                        )
                      )).values.toList(),
                      onChanged: (string) {
                        var values = _experience_values;
                        registerData.setExperience(values.indexOf(string));
                        setState(() {
                          _experience = string; 
                        });
                      },
                      style: TextStyle (
                        color: Colors.black87
                      ),
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.all(0.0),
                        prefixIcon: Icon(
                          Icons.fitness_center,
                          size: 24.0,
                        ),
                        labelText: Localization.of(context).translate("Register_Experience"),
                        labelStyle: TextStyle(
                          color: Colors.black87
                        ),
                        helperStyle: null,
                        fillColor: Colors.black87,
                        hoverColor: Colors.black87,
                        focusColor: Colors.black87,
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    InkWell(
                      onTap: () {
                        _handleRegisterTap();
                      },
                      child: Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(vertical: 13),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          color: Colors.black87,
                          borderRadius: BorderRadius.all(Radius.circular(5)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey[850],
                              blurRadius: 5.0, // has the effect of softening the shadow
                              spreadRadius: 1.0, // has the effect of extending the shadow
                              offset: Offset(
                                0.0, // horizontal, move right 10
                                5.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        child: _getRegisterButton(),
                      ),
                    ),
                    SizedBox(
                    height: 20,
                    ), 
                  ],
              )
            )
          ),   
        ]
      )
    );
  }
}



class NumberTextInputFormatter extends TextInputFormatter {
  NumberTextInputFormatter({this.decimalRange}) : assert(decimalRange == null || decimalRange > 0);

  final int decimalRange;

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    if(newValue.text.contains('-') || 
      '.'.allMatches(newValue.text).length > 1 ||
      ','.allMatches(newValue.text).length > 0) {
      return oldValue;
    }

    TextEditingValue _newValue = this.sanitize(newValue);
    String text = _newValue.text;

    if (decimalRange == null) {
      return _newValue;
    }

    if (text == '.') {
      return TextEditingValue(
        text: '0.',
        selection: _newValue.selection.copyWith(baseOffset: 2, extentOffset: 2),
        composing: TextRange.empty,
      );
    }

    return this.isValid(text) ? _newValue : oldValue;
  }

  bool isValid(String text) {
    int dots = '.'.allMatches(text).length;

    if (dots == 0) {
      return true;
    }

    if (dots > 1) {
      return false;
    }

    return text.substring(text.indexOf('.') + 1).length <= decimalRange;
  }

  TextEditingValue sanitize(TextEditingValue value) {
    if (false == value.text.contains('-')) {
      return value;
    }

    String text = '-' + value.text.replaceAll('-', '');

    return TextEditingValue(text: text, selection: value.selection, composing: TextRange.empty);
  }
}