import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Assets/S3.dart';
import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/UI/ExerciseListUI.dart';
import 'package:fitledge/UI/HomeUI.dart';
import 'package:fitledge/Widgets/CustomCard.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'ProfileUI.dart';

class MainUI extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => MainState();
}

class MainState extends State<MainUI>{
  int _selectedIndex = 0;
  bool _exerciseScreenSelected = false;

  void initState() {
    User.fetchUserData();
    retrieveExercises(Local.getLocal()).then((_) {
      retrieveDefaultSeries();
      retrieveUserSeries(User.instance.username);
      getSeriesTypesImages();
      Graphs.updateTimeIntervalDependantCharts();
    });
  }

  Widget getSearchPage() {
    if(Exercises.isLoadingExercises || Series.isLoadingDefaultSeriesImages) {
      return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.deepOrange),
              )
            ])
          ],
        )
      );
    }
    return ListView(
      children: Exercises.typeList.map((item) => FitLedgeCustomCard(item)).toList()
    );
  }

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text("FitLedge"),
        centerTitle: true,
        backgroundColor: Colors.black87,
        elevation: 1,
        leading: GestureDetector(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()));
          },
          child: Icon(Icons.person),
        ),
      ),
      body: _selectedIndex == 1 ? Container(
          padding: EdgeInsets.all(10),
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.black87,
          alignment: Alignment.center,
          child: Observer(
            builder: (_) {
              return getSearchPage();
            },
          )
      ) : Container(
        padding: EdgeInsets.all(10),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: Colors.black87,
        alignment: Alignment.center,
        child: HomeUI(),
      ),
      floatingActionButton: Container(
        height: 90.0,
        width: 90.0,
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => ExerciseListUI())); 
            },
            child: Icon(Ionicons.ios_fitness),
            backgroundColor: Colors.deepOrange,
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavigationBar(
        onTap: (int index) => setState(() => {
          _selectedIndex = index
        }),
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.deepOrange,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(Localization.of(context).translate("Home_HomeBtn")),
            backgroundColor: Colors.black87,
          ),
          
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            title: Text(Localization.of(context).translate("Home_SearchBtn")),
            backgroundColor: Colors.black87
          ),
        ],
      ),
    );
  }
}

