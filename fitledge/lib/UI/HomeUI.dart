import 'package:fitledge/Store/graphInfo.dart';
import 'package:fitledge/Widgets/Charts.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class HomeUI extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomeUIState();
}

class HomeUIState extends State<HomeUI> {
  RefreshController _refreshController = RefreshController(
    initialRefresh: false,
    initialRefreshStatus: RefreshStatus.completed 
  );
  void _onRefresh() async{
    await Graphs.updateTimeIntervalDependantCharts();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal:2.0),
      child: Center(
        child: SmartRefresher(
          enablePullDown: true,
          enablePullUp: false,
          header: ClassicHeader(
            textStyle: TextStyle(color: Colors.deepOrange),
            failedIcon: const Icon(Icons.error, color: Colors.deepOrange), 
            completeIcon: const Icon(Icons.done, color: Colors.deepOrange), 
            idleIcon: const Icon(Icons.arrow_downward, color: Colors.deepOrange), 
            releaseIcon: const Icon(Icons.refresh, color: Colors.deepOrange)
          ),
          controller: _refreshController,
          onRefresh: _onRefresh,
          child: ListView(
            children: <Widget>[
              TimeIntervalController(),
              SizedBox(height: 10,),
              SeriesCompletionChart(),
              SizedBox(height: 10),
              Row(
                children: <Widget>[
                  ExerciseTypeChart(),
                  Spacer(),
                  Expanded(
                    flex:10,
                    child: Container(
                      height: 250,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          // CaloriesBurntCard(),
                          TimeSpentCard(),
                          NumberOfSeriesDoneCard(),
                        ]
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TimeIntervalController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TimeIntervalControllerState();
}

class TimeIntervalControllerState extends State<TimeIntervalController> {
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal:2.0),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff151515),
          borderRadius: BorderRadius.all(Radius.circular(5)) 
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical:8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: () => {
                    showDatePicker(
                      context: context, 
                      initialDate: Graphs.dataBeginDate, 
                      firstDate: DateTime(2020,1,1), 
                      lastDate: DateTime.now()
                    ).then((date) {
                      if(date != null) {
                        Graphs.dataBeginDate = date;
                        Graphs.updateTimeIntervalDependantCharts();
                      }
                    })
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(left:15),
                        child: Container(
                          child: Observer(
                            builder: (context) => Text(
                              "${Graphs.dataBeginDate.day}/${Graphs.dataBeginDate.month}/${Graphs.dataBeginDate.year}",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.deepOrange
                              ),  
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 2,
                      ),
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.deepOrange,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Icon(
                  Icons.arrow_forward,
                  color: Colors.deepOrange,
                ),
              ),
              Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: () => {
                    showDatePicker(
                      context: context, 
                      initialDate: Graphs.dataEndDate, 
                      firstDate: Graphs.dataBeginDate, 
                      lastDate: DateTime.now()
                    ).then((date) {
                      if(date != null) {
                        Graphs.dataEndDate = date;
                        Graphs.updateTimeIntervalDependantCharts();
                      }
                    }),
                    },
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left:15),
                        child: Container(
                          child: Observer(
                            builder: (context) => Text(
                            "${Graphs.dataEndDate.day}/${Graphs.dataEndDate.month}/${Graphs.dataEndDate.year}",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.deepOrange
                            ),  
                          ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 2,
                      ),
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.deepOrange,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}