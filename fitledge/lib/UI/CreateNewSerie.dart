import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fitledge/UI/SeriesMetadataDetailsUI.dart';
import 'package:flutter/services.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class CreateNewSerie extends StatefulWidget {
  final String specificType;

  CreateNewSerie({Key key, this.specificType}) : super(key: key);

  @override
  _CreateNewSerieState createState() => _CreateNewSerieState(specificType);
}

class _CreateNewSerieState extends State<CreateNewSerie> {
  static String specificType;

  List<ExerciseModel> chosenExercises = [];

  Map<ExerciseModel, bool> model;
  // to reset filter
  Map<ExerciseModel, bool> copyModel;

  // to use in filter
  List<String> focos = Exercises.typeList;
  String _selectedFocus = 'Mixed';

  // constructor
  _CreateNewSerieState(String _specificType) {
    specificType = _specificType;
  }

  List<Widget> _getTitle(ExerciseModel exercise) {
    List<Widget> titleList = [
      SizedOverflowBox(
        size: Size(75,60),
        // maxHeight: 80,
        // maxWidth: 80,
        child: Image(
          image:exercise.thumbnailImage.image,
          alignment: Alignment.center,
          fit: BoxFit.fitHeight,
          width: 85,
          height: 70,
        ),
      ),
      Expanded (
        child: Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text(
            exercise.name,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
        ),
      )
    ];
    return titleList;
  }

  @override
  void initState() {
    model = new Map.fromIterables(
        Exercises.list.where((x) => x.type == specificType).toList(),
        new List<bool>.filled(
            Exercises.list.where((x) => x.type == specificType).toList().length,
            false));
    copyModel = new Map.fromIterables(
        Exercises.list.where((x) => x.type == specificType).toList(),
        new List<bool>.filled(
            Exercises.list.where((x) => x.type == specificType).toList().length,
            false));
  }

  @override
  Widget build(BuildContext context) {
    ListView newlistModel = new ListView(
      children: model.keys.map((ExerciseModel key) {
      YoutubePlayerController _controller = YoutubePlayerController(
        initialVideoId: YoutubePlayer.convertUrlToId(key.video_url),
        flags: YoutubePlayerFlags(autoPlay: false, hideThumbnail: true),
      );

      return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top:15.0),
              child: Theme(
                data: ThemeData(
                  unselectedWidgetColor: Colors.deepOrange,
                  accentColor: Colors.deepOrange
                ),
                child: Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      model[key] = value;
                      copyModel[key] = value;
                      if (value) {
                        chosenExercises.add(key);
                        } else {
                        chosenExercises.remove(key);
                        }
                      });
                    },
                  value: model[key],
                ),
              ),
            ),
          ),              
          Expanded(
            flex: 7,
            child: Card(
          color: Colors.black54,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Colors.black54,
            ),
            borderRadius: BorderRadius.circular(7.0)
          ),
          margin: EdgeInsets.symmetric(vertical:5, horizontal:5),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(7.0),
            child: ListTileTheme(
              contentPadding: EdgeInsets.only(right: 8),
              child: Theme(
                data: ThemeData(
                  unselectedWidgetColor: Colors.deepOrange,
                  accentColor: Colors.deepOrange
                ),
                child: ExpansionTile(
                  title: Row (
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: _getTitle(key)
                  ),
                  children: <Widget>[
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Description"),
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            Text(
                              "\n"+key.description,
                              style: TextStyle(
                                color: Colors.white, 
                                fontSize: 15
                              )
                            )
                          ],
                        )
                      )
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, bottom: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Type"),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            Text(
                              "\n"+key.type,
                              style: TextStyle(
                                color: Colors.white, 
                                fontSize: 15
                              )
                            )
                          ],
                        )
                      )
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, bottom: 40, right: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Video"),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            YoutubePlayer(
                              controller: _controller,
                              showVideoProgressIndicator: true,
                              progressIndicatorColor: Colors.deepOrange,
                              progressColors: ProgressBarColors(
                                playedColor: Colors.deepOrange,
                                handleColor: Colors.deepOrange[300],
                              ),
                            ),
                          ],
                        )
                      )
                    ),
                     SizedBox(
                      height: 10,
                    ),
                  ]
                ),
              )
            ),
          )
          )
          ),
        ],
      );
    }).toList());

    return Scaffold(
        appBar: AppBar(
          title: Text(
              Localization.of(context).translate("CreateSeries_Exercises")),
          centerTitle: true,
          backgroundColor: Colors.black87,
          elevation: 1,
          actions: <Widget>[
            // PopupMenuButton<String>(
            //   icon: Icon(Icons.filter_list),
            //   onSelected: (choice) {
            //     setState(() {
            //       _selectedFocus = choice;
            //       if (choice != "Mixed") {
            //         List<ExerciseModel> filteredList =
            //             Exercises.list.where((x) => x.type == choice).toList();
            //         List<bool> booleans = new List<bool>();
            //         filteredList.forEach((exercise) => {
            //               if (copyModel[exercise])
            //                 {booleans.add(true)}
            //               else
            //                 {booleans.add(false)}
            //             });
            //         model = new Map.fromIterables(filteredList, booleans);
            //       } else {
            //         model = copyModel;
            //       }
            //     });
            //   },
            //   itemBuilder: (context) {
            //     return focos.map((String foco) {
            //       return PopupMenuItem<String>(
            //         child: Text(foco),
            //         value: foco,
            //       );
            //     }).toList();
            //   },
            // ),
            GestureDetector(
                onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                SeriesRepSet(chosenExercises)));
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Icon(
                    Icons.check,
                    color: chosenExercises.length == 0 ? Colors.grey : Colors.white,
                  ),
                )),
          ],
        ),
        body: Container(color: Colors.black87, child: newlistModel)
      );
  }
}

class SeriesRepSet extends StatefulWidget {
  List<ExerciseModel> chosenExercises;
  SeriesRepSet(this.chosenExercises);

  @override
  State<StatefulWidget> createState() =>
      SeriesRepSetState(this.chosenExercises);
}

class SeriesRepSetState extends State {
  List<Map<ExerciseModel, Map<String, int>>> exercises;
  SeriesRepSetState(_chosenExercises) { 
    this.exercises = new List<Map<ExerciseModel, Map<String, int>>>();
    for(ExerciseModel exercise in _chosenExercises) {
      this.exercises.add({exercise: {
        "repetitions": 1,
        "sets":1,
      }});
    }
  }

  Future<int> _showPicker(BuildContext context, int currentValue) async {
    return await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return Theme(
            data: Theme.of(context).copyWith(
                dialogBackgroundColor: Color(0xFFF0F0F0),
                accentColor: Colors.deepOrange[800],
                textTheme: TextTheme(
                  body1: TextStyle(color: Colors.deepOrange),
                )),
            child: new NumberPickerDialog.integer(
              minValue: 1,
              maxValue: 99,
              initialIntegerValue: currentValue,
              decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.black, width: 1),
                    top: BorderSide(color: Colors.black, width: 1)),
              ),
              // title: new Text("Pick a new price"),
              cancelWidget: FlatButton(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: Colors.deepOrange[800],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      Localization.of(context).translate("MetadataSeries_Cancel"),
                      style: TextStyle(color: Color(0xFFF0F0F0)),
                    ),
                  )
                ),
              ),
              confirmWidget: FlatButton(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    color: Colors.deepOrange[800],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      Localization.of(context).translate("MetadataSeries_Confirm"),
                      style: TextStyle(color: Color(0xFFF0F0F0)),
                    ),
                  )
                ),
              ),
            ),
          );
        });
  }

  Widget _getComboBox({@required String title,
      @required int value,
      @required IconData icon,
      @required BuildContext context,
      Function(int) onChange}) {
    return GestureDetector(
      onTap: () async {
        onChange(await _showPicker(context, value));
      },
      child: Container(
        width: 75,
        decoration: BoxDecoration(
            border:Border(bottom: BorderSide(color: Colors.deepOrange, width: 1))
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              "${title}:",
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Icon(
                    icon,
                    color: Colors.deepOrange,
                    size: 24,
                  ),
                ),
                Spacer(
                  flex: 1,
                ),
                Expanded(
                  child: Text(
                    "${value}",
                    style: TextStyle(color: Colors.white, fontSize: 16)
                  ),
                ),
                Expanded(
                  child: Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.deepOrange,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    ListView exerciseRepSetCardList = new ListView.builder(
        itemCount: exercises.length,
        itemBuilder: (BuildContext context, int index) {
          ExerciseModel exercise = exercises[index].keys.toList()[0];

          List<Widget> titleList = new List<Widget>();
          titleList = [
            SizedOverflowBox(
              size: Size(75, 60),
              // maxHeight: 80,
              // maxWidth: 80,
              child: Image(
                image: exercise.thumbnailImage.image,
                alignment: Alignment.center,
                fit: BoxFit.fitHeight,
                width: 85,
                height: 70,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Container(
                width: MediaQuery.of(context).size.width / 3,
                child: Text(
                  exercise.name,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0, bottom: 8, right: 2),
                child: Row(
                    // crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      _getComboBox(
                        title: Localization.of(context)
                            .translate("CreateSeries_Reps"),
                        value: exercises[index][exercise]["repetitions"],
                        icon: Icons.repeat,
                        context: context,
                        onChange: ((value) {
                          if(value != null) {
                            setState(() {
                            this.exercises[index][exercise]["repetitions"] = value;
                          });
                          }
                        }),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      _getComboBox(
                        title: Localization.of(context)
                            .translate("CreateSeries_Sets"),
                        value: exercises[index][exercise]["sets"],
                        icon: Icons.plus_one,
                        context: context,
                        onChange: ((value) {
                          if(value != null) {
                            setState(() {
                              this.exercises[index][exercise]["sets"] = value;
                            });
                          }
                        }),
                      ),
                    ]),
              ),
            ) // AQUI REPSET
          ];

          return Card(
              color: Colors.black54,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Colors.black54,
                  ),
                  borderRadius: BorderRadius.circular(7.0)),
              margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(7.0),
                child: ListTileTheme(
                    contentPadding: EdgeInsets.only(right: 8),
                    child: Theme(
                      data: ThemeData(
                          unselectedWidgetColor: Colors.deepOrange,
                          accentColor: Colors.deepOrange),
                      child: ListTile(
                        title: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: titleList),
                      ),
                    )),
              ));
        });

    return Scaffold(
        appBar: AppBar(
          title: Text(Localization.of(context)
              .translate("CreateSeries_RepetitionsSets")),
          centerTitle: true,
          backgroundColor: Colors.black87,
          elevation: 1,
          actions: <Widget>[
            GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            SeriesUserDetailsUI(listExercises: this.exercises)
                    )
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 8.0),
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                  ),
                )),
          ],
        ),
        body: Container(color: Colors.black87, child: exerciseRepSetCardList));
  }
}
