import 'package:fitledge/Assets/Dialogs.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/UI/SeriesDetailsUI.dart';
import 'package:fitledge/Widgets/SeriesListView.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:fitledge/UI/SpecificCategoryUI.dart';


class SeriesUserUI extends StatefulWidget {
  State<StatefulWidget> createState() => SeriesUserUIState();
}

class SeriesUserUIState extends State {
  bool isEdit;

  void initState() {
    this.isEdit = false;
  }

  Widget getSeriesList(BuildContext context) {
    if(Series.isLoadingUserSeries) {
      return Container(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation(Colors.deepOrange),
              )
            ])
          ],
        )
      );
    }
    else {
      return SeriesListView(
          context, 
          Series.userList,
          false,
          onCardPressed: (SeriesModel series) { 
            Navigator.push(context, MaterialPageRoute(builder: (context) => SeriesDetailUI(series, false)));
          },
          isEdit: this.isEdit,
          onDeletePressed: (SeriesModel series) {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return SeriesDeleteConfirmDialog(series);
              }
            );
          }
        );
    }
  }

  @override
  Widget build( BuildContext context ) {
    return Scaffold(
      appBar: AppBar(
        leading: this.isEdit ? 
        GestureDetector(
          onTap: () {
            setState(() {
              this.isEdit = false;
            });
          },
          child: Icon(
            Icons.clear,
            size: 28,
          ),
        )
        :
        null,
        title: Text(Localization.of(context).translate("UserSeries_Title")),
        centerTitle: true,
        elevation: 1,
        backgroundColor: Colors.black87,
        actions: <Widget>[
           Observer(
            builder: (_) => GestureDetector(
              onTap: () {
                if(!Series.isLoadingUserSeries) {
                  setState(() {
                    this.isEdit = !this.isEdit;
                  });
                }
              },
              child: Container( 
                alignment: AlignmentDirectional.center,
                padding: EdgeInsets.only(right: 10, top: 2),
                child: Icon( 
                  Icons.edit,
                  size: 26,
                  color: Series.isLoadingUserSeries ? Colors.grey : Colors.white
                )
              )  
            ),
          ),
          Observer(
            builder: (_) => GestureDetector(
              onTap: () {
                this.isEdit = false;
                if(!Series.isLoadingUserSeries) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SpecificCategoryList()));
                }
              },
              child: Container( 
              alignment: AlignmentDirectional.center,
              padding: EdgeInsets.only(right: 10, top: 2),
              child: Icon( 
                Icons.add,
                size: 35,
                color: Series.isLoadingUserSeries ? Colors.grey : Colors.white
              )
            )  
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.black87,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Observer(
          builder: (_) => getSeriesList(context)
        )
      )
    );
  }
}