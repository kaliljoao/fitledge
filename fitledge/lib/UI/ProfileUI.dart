import 'package:camera/camera.dart';
import 'package:fitledge/Assets/Cognito.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Assets/S3.dart';
import 'package:fitledge/Store/user.dart';
import 'package:fitledge/UI/CameraScreen.dart';
import 'package:fitledge/globals.dart';
import 'package:fitledge/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:fitledge/UI/SeriesUserUI.dart';

class Profile extends StatefulWidget {
  final UserModel user;
  const Profile({Key key, this.user}) : super(key: key);

  @override
  ProfileState createState() => new ProfileState();
}

class ProfileState extends State<Profile> {
  UserModel user = User.instance;
  UserModel editedModel = new UserModel();
  bool isEditing = false;
  List<CameraDescription> _cameras;
  final List<String> _experience_values = ["Novice", "Amateur", "Intermediate", "Experienced"];


  void initState() {
    getCameras();
  }

  Future<void> getCameras() async {
    this._cameras = await availableCameras();
  }

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      return Scaffold(
        appBar: new AppBar(
          title: Text(Localization.of(context).translate("Profile_Title")),
          centerTitle: true,
          backgroundColor: Colors.black87,
          elevation: 1,
          actions: <Widget>[
            GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/userSeries");
                },
                child: Container(
                  alignment: AlignmentDirectional.center,
                  padding: EdgeInsets.only(right: 15),
                  height: AppBar().preferredSize.height,
                  child: Text(
                    Localization.of(context).translate("Profile_Series"),
                    style: new TextStyle(color: Colors.white, fontSize: 18),
                    textAlign: TextAlign.center,
                  ),
                ))
          ],
        ),
        body: getWidget(),
      );
    });
  }

  Widget getWidget() {
    if (user.isLoadingUserData) {
      return Container(
          color: Colors.black87,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation(Colors.deepOrange),
                )
              ])
            ],
          ));
    }
    return profileCard();
  }

  Widget profileCard() {
    return Container(
      color: Colors.black87,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 55,
          ),
          GestureDetector(
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CameraScreen(camera: _cameras[0],))),
            child: Stack(
              children: [
                new Container(
                  width: 100.0,
                  height: 100.0,
                  decoration: new BoxDecoration (
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    shape: BoxShape.rectangle,
                    image: new DecorationImage(
                      fit: BoxFit.fill,
                      image: User.instance.profileImage, 
                    )
                  )
                ),
                Positioned(
                  right: 2,
                  bottom: 2,
                  child: Icon(
                    Icons.camera_alt,
                    color: Colors.black,
                  ),
                ),
              ]
            )
          ),
          SizedBox(
            height: 55,
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: new BoxDecoration(
                border:
                    Border(bottom: BorderSide(color: Colors.grey, width: 0.4)),
              ),
              padding: EdgeInsets.only(right: 8),
              child: Align(
                alignment: FractionalOffset.topRight,
                child: GestureDetector(
                  onTap: () {
                    setState(() {
                      if (!isEditing)
                        isEditing = true;
                      else {
                        user.setIsLoadingUserData(true);
                        isEditing = false;
                        editedModel.username = user.username;
                        updateUser(editedModel);
                      }
                    });
                  },
                  child: Icon(
                    Icons.edit,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          isEditing == false
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      Localization.of(context).translate("Register_Fullname"),
                      style: style,
                    ),
                    Text(
                      user.name ?? "",
                      style: style,
                    )
                  ],
                ))
            : Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    Localization.of(context).translate("Register_Fullname"),
                    style: style,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  new Expanded(
                      child: TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: user.name,
                        hintStyle: TextStyle(color: Colors.white)),
                    onChanged: (newValue) {
                      setState(() {
                        editedModel.setName(newValue);
                      });
                    },
                  ))
                ],
              )
            ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: new Divider(
              color: Colors.grey,
            ),
          ),
          isEditing == false
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "E-mail",
                      style: style,
                    ),
                    Text(
                      user.email ?? "",
                      style: style,
                    )
                  ],
                ))
            : Container(
              padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "E-mail",
                    style: style,
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  new Expanded(
                      child: TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: user.email,
                        hintStyle: TextStyle(color: Colors.white)),
                    onChanged: (newValue) {
                      setState(() {
                        editedModel.setEmail(newValue);
                      });
                    },
                  ))
                ],
              )
            ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: new Divider(
              color: Colors.grey,
            ),
          ),
          isEditing == false
              ? Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Height"),
                        style: style,
                      ),
                      Text(
                        user.height.toString() + " m" ?? "",
                        style: style,
                      )
                    ],
                  ))
              : Container(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Height"),
                        style: style,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      new Expanded(
                          child: TextField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: user.height.toString(),
                            hintStyle: TextStyle(color: Colors.white)),
                        onChanged: (newValue) {
                          setState(() {
                            editedModel.setHeight(double.parse(newValue));
                          });
                        },
                      ))
                    ],
                  )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: new Divider(
              color: Colors.grey,
            ),
          ),
          isEditing == false
              ? Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Weight"),
                        style: style,
                      ),
                      Text(
                        user.weight.toString() + " Kg" ?? "",
                        style: style,
                      )
                    ],
                  ))
              : Container(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Weight"),
                        style: style,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      new Expanded(
                          child: TextField(
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: user.weight.toString(),
                            hintStyle: TextStyle(color: Colors.white)),
                        onChanged: (newValue) {
                          setState(() {
                            editedModel.setWeight(double.parse(newValue));
                          });
                        },
                      ))
                    ],
                  )),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: new Divider(
              color: Colors.grey,
            ),
          ),
          isEditing == false ? Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Experience"),
                        style: style,
                      ),
                      Text(
                        _experience_values[user.experience],
                        style: style,
                      )
                    ],
                  ))
              : Container(
                  padding: EdgeInsets.symmetric(vertical: 0, horizontal: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        Localization.of(context).translate("Register_Experience"),
                        style: style,
                      ),
                      SizedBox(
                        width: 30,
                      ),
                      new Expanded(
                        child: DropdownButtonFormField(
                          value: _experience_values[user.experience],
                          items: _experience_values.asMap()
                          .map((index,label) => MapEntry(index,  
                            DropdownMenuItem(
                              child: Text(
                                label, 
                                style: TextStyle(
                                  color: _experience_values[user.experience] == label ? Colors.deepOrange : Colors.black
                                ),
                              ),
                              value: label,
                              key: Key(label),
                            )
                          )).values.toList(),
                          onChanged: (string) {
                            var values = _experience_values;                               
                            setState(() {
                              user.experience = (values.indexOf(string));
                              editedModel.experience = (values.indexOf(string));
                            });                         
                          },
                          style: TextStyle (
                            color: Colors.white
                          ),
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.all(0.0),
                            helperStyle: null,
                            fillColor: Colors.white,
                            hoverColor: Colors.white,
                            focusColor: Colors.white,
                          ),
                        ),
                      )
                    ],
                  )),
          Expanded(
            child: Align(
              alignment: FractionalOffset.bottomCenter,
              child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: 65,
                  child: GestureDetector(
                    onTap: () {
                      signOut();
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (_) => MyLoginPage()),
                          (_) => false);
                    },
                    child: Container(
                      color: Colors.black38,
                      child: Center(
                        child: Text(
                          Localization.of(context).translate("Profile_Logout"),
                          style: TextStyle(
                              fontSize: 20,
                              color: Color.fromRGBO(128, 0, 0, 1)),
                        ),
                      ),
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }
  final TextStyle style = new TextStyle(color: Colors.white, fontSize: 16);
}
