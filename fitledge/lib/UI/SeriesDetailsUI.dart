import 'package:fitledge/Assets/Dialogs.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/UI/SeriesMetadataDetailsUI.dart';
import 'package:fitledge/Widgets/CircularProgressIndicator.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:percent_indicator/percent_indicator.dart';

class SeriesDetailUI extends StatefulWidget {
  SeriesModel series;
  bool isPractice;
  SeriesDetailUI(this.series, this.isPractice);

  SeriesDetailState createState() => SeriesDetailState(series, isPractice);
}

class SeriesDetailState extends State<SeriesDetailUI> {
  SeriesModel series;
  bool isPractice;
  SeriesDetailState(this.series, this.isPractice);
  bool isAddSeries = false;

  void initState() {
    bool found = false;
    for(SeriesModel series in Series.userList){
      if(series.id == this.series.id) {
        found = true;
        break;
      }
    }
    this.isAddSeries = !found;
  }

  Widget getSeriesAddButton() {
    if(isAddSeries) {
      return GestureDetector(
        onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => SeriesUserDetailsUI(series:series)));
          },
          child: Container(
            alignment: AlignmentDirectional.center,
            padding: EdgeInsets.only(right: 10, top: 2),
            child: Icon( 
              Icons.add,
              size: 35,
            )
          )  
      );
    }
    else {
      return Container(); // #TODO Edit series
    }
  }

  Widget build(BuildContext context) {
    ListView newlistModel = new ListView.builder(
      itemCount: series.seriesExercises.length,
      itemBuilder:  (BuildContext context, int index) {

        YoutubePlayerController _controller = YoutubePlayerController(
            initialVideoId: YoutubePlayer.convertUrlToId(series.seriesExercises[index].keys.toList()[0].video_url),
            flags: YoutubePlayerFlags(
                autoPlay: false,
                hideThumbnail: true
            ),
        );

        List<Widget> titleList = new List<Widget>();

        ExerciseModel exercise =  series.seriesExercises[index].keys.toList()[0];
        bool exerciseDone = statusExists(seriesId: series.id, exerciseName: exercise.name, status: "exercise_end");
        if(isPractice == true){
          titleList = [
            SizedOverflowBox(
              size: Size(75,60),
              // maxHeight: 80,
              // maxWidth: 80,
              child: Image(
                image:exercise.thumbnailImage.image,
                alignment: Alignment.center,
                fit: BoxFit.fitHeight,
                width: 85,
                height: 70,
              ),
            ),
            Expanded(
              flex: 10,
              child: Padding(
                padding: const EdgeInsets.only(left:15.0),
                child: Text(
                 exercise.name,
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
              ),
            ),
            exerciseDone ? 
            Expanded(
              flex: 1,
              child: CircularPercentIndicator( 
                radius: 35.0,
                lineWidth: 3.0,
                percent: 1,
                progressColor: Colors.green[800],
              ),
            )
            :
            Expanded(
              flex: 1,
              child: CircularPercentIndicator( 
                radius: 35.0,
                lineWidth: 3.0,
                percent: getExerciseCompleteness(series: series, exercise:exercise),
                progressColor: Colors.yellow,
              ),
            )
          ];
        }
        else {
          titleList = [
            SizedOverflowBox(
              size: Size(75,60),
              // maxHeight: 80,
              // maxWidth: 80,
              child: Image(
                image:exercise.thumbnailImage.image,
                alignment: Alignment.center,
                fit: BoxFit.fitHeight,
                width: 85,
                height: 70,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left:15.0),
              child: Text(
                series.seriesExercises[index].keys.toList()[0].name,
                style: TextStyle(color: Colors.white, fontSize: 20),
              ),
            ),
            Expanded(child: Container(),)
          ];
        }

        return Card(
          color: Colors.black54,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: Colors.black54,
            ),
            borderRadius: BorderRadius.circular(7.0)
          ),
          margin: EdgeInsets.symmetric(vertical:5, horizontal:5),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(7.0),
            child: ListTileTheme(
              contentPadding: EdgeInsets.only(right: 8),
              child: Theme(
                data: ThemeData(
                  unselectedWidgetColor: Colors.deepOrange,
                  accentColor: Colors.deepOrange
                ),
                child: ExpansionTile(
                  title: Row (
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: titleList
                  ),
                  children: <Widget>[
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Description"),
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            Text(
                              "\n"+series.seriesExercises[index].keys.toList()[0].description,
                              style: TextStyle(
                                color: Colors.white, 
                                fontSize: 15
                              )
                            )
                          ],
                        )
                      )
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, bottom: 10),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Type"),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            Text(
                              "\n"+series.seriesExercises[index].keys.toList()[0].type,
                              style: TextStyle(
                                color: Colors.white, 
                                fontSize: 15
                              )
                            )
                          ],
                        )
                      )
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20, bottom: 40, right: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              Localization.of(context).translate("UserSeries_Video"),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.deepOrange, 
                                fontSize: 17,
                              )
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            YoutubePlayer(
                              controller: _controller,
                              showVideoProgressIndicator: true,
                              progressIndicatorColor: Colors.deepOrange,
                              progressColors: ProgressBarColors(
                                playedColor: Colors.deepOrange,
                                handleColor: Colors.deepOrange[300],
                              ),
                            ),
                          ],
                        )
                      )
                    ),
                     SizedBox(
                      height: 10,
                    ),
                    isPractice == true ? Padding(
                      padding: EdgeInsets.only(left: 20, bottom: 40, right: 20),
                      child: Align(
                        alignment: Alignment.center,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () async {
                                if(!exerciseDone) {
                                  int bateria;
                                  try {
                                    bateria = await Battery.getBattery;
                                  }
                                  catch(e) {}
                                  if(bateria != null && bateria <= 15) {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return LowBatteryExerciseDialog(
                                          bateria,
                                          series.seriesExercises[index].keys.toList()[0],
                                          series,
                                          series.seriesExercises[index].values.toList()[0]
                                        );
                                      }
                                    );
                                  }
                                  else {
                                    Navigator.push(
                                      context, 
                                      MaterialPageRoute(
                                        builder: (context) => ExerciseCircularIndicator(
                                        exercise: series.seriesExercises[index].keys.toList()[0],
                                        series: series,
                                        listRepetitions: series.seriesExercises[index].values.toList()[0],
                                        )
                                      )
                                    );
                                  }
                                }
                              },
                              child: SizedBox(
                                width: 250,
                                height: 65,
                                child: Card(
                                  color: exerciseDone ? Colors.green[800] : Colors.deepOrange,
                                  child: Center(
                                    child:  Text(
                                      exerciseDone ? Localization.of(context).translate("UserSeries_Done") : Localization.of(context).translate("UserSeries_Execute"),
                                      style: TextStyle(
                                        fontSize: 20,
                                        color: Colors.white
                                      ),
                                    )
                                  ),
                                ),
                              )
                            )
                          ],
                        )
                      )
                    ) : SizedBox(),
                  ]
                ),
              )
            ),
          )
        );
      }
    );

    return Scaffold(
      appBar: new AppBar(
          title: Text(series.name),
          centerTitle: true,
          backgroundColor: Colors.black87,
          elevation: 1,
          actions: <Widget>[
            getSeriesAddButton()
        ],
      ),
      body: Container(
        color: Colors.black87,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: newlistModel
      )
    );
  }
}