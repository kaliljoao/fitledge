import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Widgets/SeriesListView.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SeriesList extends StatefulWidget {
  String seriesType;
  SeriesList(this.seriesType);
  @override
  SeriesListState createState() => new SeriesListState(this.seriesType);
}

class SeriesListState extends State<SeriesList> {
  String seriesType;
  List<SeriesModel> series;

  SeriesListState(this.seriesType);
  
  @override
  void initState() {
    this.series = fetchSeriesFromType(seriesType);
  }

  List<SeriesModel> fetchSeriesFromType(String seriesType) {
    // This function will iterate through the series and find series
    // that contain exercises with the same type of seriesType
    // TODO Change this function to return series that are MOSTLY of the seriesType

    List<SeriesModel> returnList = [];

    for(SeriesModel series in Series.list ) {
      for(var seriesExercise in series.seriesExercises) /* Iterate series exercises */ {
        ExerciseModel exercise = seriesExercise.keys.toList()[0];
        if(exercise.type == this.seriesType) /* Series contains at least one exercise with  */ {
          returnList.add(series);
          break; // go to next series
        }
      }
    }

    return returnList;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(seriesType + " Series"),
        centerTitle: true,
        backgroundColor: Colors.black87,
        elevation: 1,
      ),
      body: Container(
        color: Colors.black87,
        child: SeriesListView(context, this.series, false)
      ) 
    );
  }
}