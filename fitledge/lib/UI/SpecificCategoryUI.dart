import 'package:fitledge/Assets/Localizations.dart';
import 'package:flutter/material.dart';

import 'package:fitledge/Store/series.dart';
import 'package:fitledge/globals.dart';
import 'package:fitledge/UI/CreateNewSerie.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class SpecificCategoryList extends StatelessWidget{
    Widget getListView(){
      List<String> types = Exercises.typeList;
      return ListView.builder(
        itemCount: types.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            color: Colors.black54,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(7.0),
            ),
            child: ListTile(
              contentPadding: EdgeInsets.symmetric(vertical:10, horizontal: 20),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => CreateNewSerie(specificType: types[index])));
              },
              title: Text(
                types[index],
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                ),
              ),
              
            )
          );
        }
      );
    }
      
  @override
  Widget build(BuildContext context)
  {
    
    return Scaffold(
      appBar: AppBar(
        title: Text(Localization.of(context).translate("CreateSeries_Title")),
        centerTitle: true,
        backgroundColor: Colors.black87,
        elevation: 1,
      ),
      body: Container(
        color: Colors.black87,
        child: Column(
          children: <Widget>[
            SizedBox(height: 10,),
            Text(
              Localization.of(context).translate("CreateSeries_Choose"),
              style: Theme.of(context).textTheme.headline.copyWith(color:Colors.white, fontWeight: FontWeight.w500),
            ),
            SizedBox(height: 10,),
            Expanded(child: Observer(builder: (_) => getListView())),
          ],
        ),
      )
    );
  }
}