import 'dart:ui';

import 'package:fitledge/Assets/Cognito.dart';
import 'package:fitledge/Assets/Dialogs.dart';
import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'package:internationalization/internationalization.dart';

class LoginUI extends StatefulWidget {
  LoginUI({
    Key key,
  }) : super(key: key);

  @override
  _LoginUIState createState() => _LoginUIState();
}

class _LoginUIState extends State<LoginUI> {
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  bool _isLoading;

  void initState() {
    _usernameController = new TextEditingController();
    _passwordController = new TextEditingController();
    _isLoading = false;
  }

  void _handleLoginTap() async {
    setState(() {
      _isLoading = true;
    });

    loginUser(_usernameController.text, _passwordController.text).then((_) {
      _redirectToHome();
      setState(() {
        _isLoading = false;
      });
    }).catchError((exc) async {
      setState(() {
        _isLoading = false;
      });

      if (exc.name == "UserNotConfirmedException") {
        resendConfirmationEmail(_usernameController.text);
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return WaitingConfirmationDialog(_usernameController.text);
            });
        await waitConfirmation(
            _usernameController.text, _passwordController.text);
        _redirectToHome();
      } 
      else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AuthenticationErrorDialog(exc);
          }
        );
      }
    });
  }

  void _redirectToHome() {
    Navigator.pushNamedAndRemoveUntil(context, "/home", (_) => false);
  }

  Widget _getLoadingButton() {
    if (!_isLoading) {
      return Text(
        'Login',
        style: TextStyle(fontSize: 20, color: Colors.deepOrange),
      );
    } else {
      return Container(
        width: 24,
        child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange),),
      );
    }
  }

  @override
  Widget build(BuildContext context) {

    return new Container(
      padding: EdgeInsets.all(20),
      color: Colors.black87,
      child: ListView(
        children: <Widget>[
          Center(
            child: Image.asset("assets/images/logo.png")
          ),
          SizedBox(
            height: 60,
          ),
          Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25)
            ),
            margin: EdgeInsets.all(20.0),
            child: Container(
              padding: EdgeInsets.only(left: 25, right: 25, top: 25, bottom: 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    style: TextStyle(color: Colors.black87),
                    controller: _usernameController,
                    cursorColor: Colors.black87,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.people),
                      labelText: Localization.of(context).translate("Login_Username"),
                      labelStyle: TextStyle(
                        color: Colors.black87
                      ),
                      fillColor: Colors.black87,
                      hoverColor: Colors.black87,
                      focusColor: Colors.black87
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  TextFormField(
                    obscureText: true,
                    style: TextStyle(color: Colors.black87),
                    controller: _passwordController,
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.vpn_key),
                      labelText: Localization.of(context).translate("Login_Password"),
                      labelStyle: TextStyle(
                        color: Colors.black87
                      ),
                      fillColor: Colors.black87,
                      hoverColor: Colors.black87,
                      focusColor: Colors.black87,
                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                  InkWell(
                    onTap: (){
                      _handleLoginTap();
                    },
                    child: Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 13),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.black87,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[850],
                            blurRadius: 5.0, // has the effect of softening the shadow
                            spreadRadius: 1.0, // has the effect of extending the shadow
                            offset: Offset(
                              0.0, // horizontal, move right 10
                              5.0, // vertical, move down 10
                            ),
                          )
                        ],
                      ),
                      child: _getLoadingButton()
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: InkWell(
                      onTap: () { Navigator.pushNamed(context, "/register/1");},
                      child: Text(
                        Localization.of(context).translate("Login_RegisterNow"),
                        style: TextStyle(
                          color: Colors.black87,
                          fontStyle: FontStyle.italic // colocar underline
                        ),
                      ),
                    )
                  )
                ],
              ),
            )
          ),
        ],
      ),
    );
  }
}
