import 'dart:ui';

import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Assets/Cognito.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Widgets/CircularProgressIndicator.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class WaitingConfirmationDialog extends StatefulWidget {
  final String username;
  WaitingConfirmationDialog(this.username);

  @override
  _WaitingConfirmationDialogState createState() => _WaitingConfirmationDialogState(username);
}

class _WaitingConfirmationDialogState extends State {
  bool _resendTimeout;
  int _resendTimeoutInterval = 30;
  final String username;

  @override
  void initState() {
    _resendTimeout = false;
  }

  _WaitingConfirmationDialogState(this.username) {
    Future.delayed(Duration(seconds:_resendTimeoutInterval), () {
      setState(() {
        _resendTimeout = true;
      });
    });
  }

  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: () async => false,
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 5,
          sigmaY: 5 
        ),
        child: SimpleDialog(
          backgroundColor: Colors.white,
          title: Text("Waiting confirmation"),
          children: <Widget>[
            Center(
            child:  Padding(
              padding: EdgeInsets.all(10.0),
              child:Row (children: <Widget>[
                Expanded (
                  flex: 4,
                  child: Text("A confirmation e-mail has been sent to you, please confirm to login to your account."),
                ),
                Flexible (
                  flex: 1,
                  child: Container(
                    child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue),),
                  ) 
                ),
              ]),  
              )    
            ),
            if(_resendTimeout == true) ...[
              Center(
                child: FlatButton(
                  color: Colors.deepOrange,
                  textColor: Colors.white,
                  padding: EdgeInsets.all(5.0),
                  child: Text("Resend email"),
                  onPressed: () {
                    resendConfirmationEmail(username);
                    setState(() {
                      _resendTimeout = false;
                    });
                    Future.delayed(Duration(seconds:_resendTimeoutInterval), () {
                      setState(() {
                        _resendTimeout = true;
                      });
                    });
                    },
                ),
              )
            ]
          ],
        )
      ) 
    );
  }
}

class ErrorMessageDialog extends StatelessWidget {
  final String message;
  ErrorMessageDialog(this.message);

  @override
  Widget build (BuildContext context) {
    return new BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5,
        sigmaY: 5 
      ),
      child: SimpleDialog(
        backgroundColor: Colors.white,
        title: Center(child: Text("Error")), 
        children: <Widget>[
          Center(
            child:  Padding(
              padding: EdgeInsets.all(20.0),
              child: Text(this.message)
            )
          ),
          Center(
            child: FlatButton(
              color: Colors.deepOrange,
              textColor: Colors.white,
              child: Text("Close"),
              onPressed: () {Navigator.pop(context);},
            ),
          )
        ],
      )
    );
  }
}

class AuthenticationErrorDialog extends StatelessWidget {
  var exception = null;
  AuthenticationErrorDialog(Exception this.exception);

  String _getExceptionMessage() {
    if (exception.code == "InvalidParameterException") {
      return "Make sure to fill all fields.\nPasswords must be at least 6 digits long and contain a uppercase, lowercase and a number.";
    }

    return exception.message;
  }

  @override
  Widget build (BuildContext context) {
    return new BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5,
        sigmaY: 5 
      ),
      child: SimpleDialog(
        backgroundColor: Colors.white,
        title: Center(child: Text("Error")), 
        children: <Widget>[
          Center(
            child:  Padding(
              padding: EdgeInsets.all(20.0),
              child: Text(_getExceptionMessage())
            )
          ),
          Center(
            child: FlatButton(
              color: Colors.deepOrange,
              textColor: Colors.white,
              child: Text("Close"),
              onPressed: () {Navigator.pop(context);},
            ),
          )
        ],
      )
    );
  }
}

class SeriesDeleteConfirmDialog extends StatelessWidget {
  SeriesModel series;

  SeriesDeleteConfirmDialog(this.series);

  @override
  Widget build (BuildContext context) {
    return new BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5,
        sigmaY: 5 
      ),
      child: SimpleDialog(
        backgroundColor: Colors.white,
        title: Center(child: Text("Confirm delete")), 
        children: <Widget>[
          Center(
            child:  Padding(
              padding: EdgeInsets.all(20.0),
              child: RichText(
                text: TextSpan(
                  text: 'Are you sure you want to delete the series ',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: '${series.name}', style: TextStyle(
                      fontWeight: FontWeight.bold, 
                      color:Colors.deepOrange, 
                      fontSize: 18)
                    ),
                    TextSpan(
                      text: '?',
                      style: TextStyle(
                    color: Colors.black,
                    fontSize: 18
                  ),
                    )
                  ],
                ),
              )
            )
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              FlatButton(
                color: Colors.deepOrange,
                textColor: Colors.white,
                child: Text("Close"),
                onPressed: () {Navigator.pop(context);},
              ),
              FlatButton(
                color: Colors.red[800],
                textColor: Colors.white,
                child: Text("Delete"),
                onPressed: () {
                  deleteSeriesFromUser(series);
                  Series.removeUserSeries(series);
                  Navigator.pop(context);
                },
              ),
            ]
          )
        ],
      )
    );
  }

}

class LowBatteryExerciseDialog extends StatelessWidget {
  int batteryLevel;
  ExerciseModel exercise;
  SeriesModel series;
  Map<String, dynamic> listRepetitions;
  LowBatteryExerciseDialog(this.batteryLevel, this.exercise, this.series, this.listRepetitions);

  @override
  Widget build (BuildContext context) {
    return new BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5,
        sigmaY: 5 
      ),
      child: SimpleDialog(
        backgroundColor: Colors.white,
        title: Center(child: Text("Warning", style: TextStyle(color: Colors.deepOrange[800]),)), 
        children: <Widget>[
          Center(
            child:  Padding(
              padding: EdgeInsets.all(20.0),
              child:  RichText(
                text: TextSpan(
                  text: 'Your battery is at ',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: '${this.batteryLevel}', style: TextStyle(
                      fontWeight: FontWeight.bold, 
                      color: this.batteryLevel <= 5 ? Colors.red[800] : Colors.deepOrange, 
                      fontSize: 18)
                    ),
                    TextSpan(
                      text: ', you may end up loosing exercising progress if your phone shut down',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18
                      ),
                    )
                  ],
                ),
              )
            
            )
          ),
          Center(
            child: FlatButton(
              color: Colors.deepOrange,
              textColor: Colors.white,
              child: Text("Continue"),
              onPressed: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context) => ExerciseCircularIndicator(
                    exercise: this.exercise,
                    series: this.series,
                    listRepetitions: this.listRepetitions,
                    )
                  )
                );
              },
            ),
          )
        ],
      )
    );
  }
}

