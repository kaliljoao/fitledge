import 'dart:convert';
import 'dart:ui';

import 'package:fitledge/Assets/S3.dart';
import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Store/user.dart';
import 'package:fitledge/Store/register.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter_aws_s3_client/flutter_aws_s3_client.dart';
import 'package:postgres/postgres.dart';

String database_url =  "fitledge-db.cnlv9u1nvsv9.us-east-2.rds.amazonaws.com";
int port = 5432;

// User queries
void insertUser(RegisterData registerData) async {
  PostgreSQLConnection conn = await openConnection();
  String sql = """insert into 
  user_info (username, name, sex, age, weight, height, experience, email) 
  values (@username, @name, @sex, @age, @weight, @height, @experience, @email)"""; 
  await conn.query(sql, 
  substitutionValues: {
    "username": registerData.username,
    "name": registerData.name,
    "sex": registerData.gender[0],
    "age": registerData.age,
    "weight": registerData.weight,
    "height": registerData.height,
    "experience": registerData.experience,
    "email": registerData.email,
  });
  conn.close();
}

Future<void> retrieveUserInfo() async {
  return await new Future(() async {
    UserModel user = User.instance;
    PostgreSQLConnection conn = await openConnection();
    
    String sql = """select * 
    from user_info 
    where username = @username""";
    
    var result = await conn.mappedResultsQuery(sql, 
    substitutionValues: {
      "username": user.username,
    });
    conn.close();
    user.setEmail(result[0]["user_info"]["email"]);
    user.setName(result[0]["user_info"]["name"]);
    user.setGender(result[0]["user_info"]["sex"]);
    user.setAge(result[0]["user_info"]["age"]);
    user.setHeight(result[0]["user_info"]["height"]);
    user.setWeight(result[0]["user_info"]["weight"]);
    user.setExperience(result[0]["user_info"]["experience"]);
    user.setIsLoadingUserData(false);
  });
}

Future<void> updateUser(UserModel model) async {
  UserModel user = User.instance;
  PostgreSQLConnection conn = await openConnection();
  
  StringBuffer buffer = new StringBuffer();

  int counterProperties = 0;
  
  buffer.write("UPDATE user_info SET ");
  
  if (model.name != null) {
    counterProperties++; 
    buffer.write("name='${model.name}'");
  }

  if (model.height != null) {
    if(counterProperties > 0)
      buffer.write(", height=${model.height}");
    else
      buffer.write("height=${model.height}");
    counterProperties++;
  }

  if (model.weight != null) {
    if(counterProperties > 0)
      buffer.write(", weight=${model.weight}");
    else
      buffer.write("weight=${model.weight}");
    counterProperties++;
  }
    
  if (model.gender != null) {
    if(counterProperties > 0)
      buffer.write(", gender='${model.gender}'");
    else
      buffer.write("gender='${model.gender}'");
    counterProperties++;
  }
    
  if (model.experience != null) {
    if(counterProperties > 0)
      buffer.write(", experience='${model.experience}'");
    else
      buffer.write("experience='${model.experience}'");
    counterProperties++;
  }
    
  
  buffer.write(" WHERE username='${model.username}';");

  String completedQuery = buffer.toString();

  if(completedQuery.contains(" name") ||
     completedQuery.contains("height") || 
     completedQuery.contains("weight") || 
     completedQuery.contains("gender") || 
     completedQuery.contains("experience")
  ) {
    var result = await conn.query(completedQuery);
    conn.close();
  }
  retrieveUserInfo();
}

// Exercise queries
Future<void> retrieveExercises(Locale local) async {
  PostgreSQLConnection conn = await openConnection();
  String sql;
  if(local.toString() != "en_US")
    sql = """select e.name, e.focus, e.type, e.video_url, e.fotos_url, e.difficulty , e.unit , e.repetition_duration , e.default_repetitions , e.default_sets , e.repetition_reverse_duration , lang.description 
      from public.exercise as e inner join public.exercise_desc_language as lang on 
      e."name" = lang.exercise where lang.language_name = '${local.toString()}';""";
  else
    sql = "select * from exercise;";
  
  var result = await conn.mappedResultsQuery(sql);
  conn.close();
  AwsS3Client s3Client = await getS3Client();
  for(final row in result) {
    var value = row["exercise"];
    var desc = row["exercise_desc_language"];

    ExerciseModel exercise;
    if(local.toString() != "en_US") {
      exercise = ExerciseModel(
        value["name"],
        value["focus"],
        value["type"],
        desc["description"],
        value["video_url"],
        value["fotos_url"],
        value["difficulty"],
        value["unit"],
        value["repetition_duration"],
        value["repetition_reverse_duration"],
      );
    }
    else{
      exercise = ExerciseModel(
        value["name"],
        value["focus"],
        value["type"],
        value["description"],
        value["video_url"],
        value["fotos_url"],
        value["difficulty"],
        value["unit"],
        value["repetition_duration"],
        value["repetition_reverse_duration"],
      );
    }
    exercise.setThumbnailImage(
      await getExerciseThumbnailImage(exercise, s3Client)
    );
    Exercises.append(exercise);
  }
  Exercises.isLoadingExercises = false;
}

// Series queries
Future<void> retrieveDefaultSeries() async {
  PostgreSQLConnection conn = await openConnection();
  String sql = """select * 
    from series 
    where user_created=false""";
  
  var result = await conn.mappedResultsQuery(sql);
  conn.close();

  for(final row in result) {
    var value = row["series"];
    SeriesModel series = SeriesModel(
      value["id"],
      value["name"],
      value["duration"],
      daysString: value["days"]
    );
    await series.loadSeriesExercises();
    Series.append(series);
  }
  Series.isLoadingSeries = false;
}

// User_series queries
Future retrieveUserSeries(String username) async {
  PostgreSQLConnection conn = await openConnection();
  String sql = """
  select s.id,s.days,s.duration,s.name,s.user_created
  from series as s, (select * 
  from user_series
  where username=@username) as user_series
  where s.id=user_series.series_id and
  is_active is true
  """;
  
  var result = await conn.mappedResultsQuery(sql,
  substitutionValues: {
    "username": User.instance.username
  });
  conn.close();

  for(final row in result) {
    var value = row["series"];
    SeriesModel series = SeriesModel(
      value["id"],
      value["name"],
      value["duration"],
      daysString:value["days"],
    );
    Series.appendUserSeries(series);
    await series.loadSeriesExercises();
  }
  Series.isLoadingUserSeries = false;

  retrieveTodaySeriesCompleteness(); // Require user's series
}

Future<void> insertUserSeries(SeriesModel model) async {
  UserModel user = User.instance;
  PostgreSQLConnection conn = await openConnection();
  String query = "INSERT INTO user_series values ('${user.username}', ${model.id});";
  await conn.query(query);
  conn.close();
}

Future<int> insertUserCreatedSeries(SeriesModel model, List<Map<ExerciseModel, dynamic>> exercises) async {
  UserModel user = User.instance;
  PostgreSQLConnection conn = await openConnection();
  int idSerie;
  
  String sqlSerie = "INSERT INTO series (days, duration, name, user_created) VALUES ('${model.days.join(",")}', '${model.duration}', '${model.name}', TRUE );";
  await conn.query(sqlSerie);
  
  List<dynamic> ids = await conn.query("SELECT id FROM series WHERE name='${model.name}';");
  for(final row in ids) {
    idSerie = row[0];
  }

  await _insertSeriesExercises(exercises, conn, idSerie);

  String sqlUserSerie = "insert into user_series values ('${user.username}', ${idSerie});";
  await conn.query(sqlUserSerie);
  conn.close();
  return idSerie;
}

Future<bool> _insertSeriesExercises(List<Map<ExerciseModel, dynamic>> exercises, PostgreSQLConnection conn, int serieId) async {
  for(var i = 0; i < exercises.length; i++) {
    String sqlSerieExercise = "INSERT INTO series_exercises (series_id, exercise_name, repetitions, sets) VALUES ($serieId, '${exercises[i].keys.toList()[0].name}', ${exercises.where((x) => x.containsKey(exercises[i].keys.toList()[0])).toList()[0][exercises[i].keys.toList()[0]]['repetitions']}, ${exercises.where((x) => x.containsKey(exercises[i].keys.toList()[0])).toList()[0][exercises[i].keys.toList()[0]]['sets']} );";
    await conn.query(sqlSerieExercise);
  }
}

Future<List<Map<ExerciseModel,dynamic>>> retrieveSeriesExercises(int id) {
  return new Future(() async {
    PostgreSQLConnection conn = await openConnection();
    String sql = """select * 
    from series_exercises
    where series_id=@id""";
    
    var result = await conn.mappedResultsQuery(sql,
    substitutionValues: {
      "id":id,
    });
    conn.close();

    List<Map<ExerciseModel,dynamic>> seriesExercises = [];

    for(final row in result) {
      var value = row["series_exercises"];
      for(ExerciseModel exercise in Exercises.list) {
        if(exercise.name == value["exercise_name"]) {
          seriesExercises.add({
            exercise: {
              "repetitions": value["repetitions"],
              "sets": value["sets"],
            }
          });
        }
      }
    }
    return seriesExercises;
  });
}

Future<void> deleteSeriesFromUser(SeriesModel series) {
  openConnection().then((conn) {
    String query = """
      update user_series
      set is_active = false
      where series_id = ${series.id} and
      username = '${User.instance.username}'
    """;
    conn.execute(query);
  });

}

// Series_completeness queries
Future<void> retrieveTodaySeriesCompleteness() async {
  PostgreSQLConnection conn = await openConnection();
  String completedSeriesQuery = """
  select *
  from user_series_accomplishment
  where date = now()::date and
  username = '${User.instance.username}' and 
  done is true
  """;

  var resultCompletedSeries = await conn.mappedResultsQuery(completedSeriesQuery);

  for(final row in resultCompletedSeries) {
    var value = row["user_series_accomplishment"];
    SeriesModel series = Series.userList.where((SeriesModel series){
      return series.id == value["series"];
    }).toList()[0];
    ExercisesCompleteness.appendCompletedSeries(series);
  }
  
  String todaySeriesExercisesCompleteness = """
    select * 
    from user_series_accomplishment_log
    where timestamp <= now() and timestamp >= now()::date::timestamp and -- timestamp between 00:00 of today and now 
    username = '${User.instance.username}'
  """;

  var resultTodaySeriesExecisesCompleteness = await conn.mappedResultsQuery(todaySeriesExercisesCompleteness);

  conn.close();

  for(final row in resultTodaySeriesExecisesCompleteness) {
    var value = row["user_series_accomplishment_log"];
    SeriesModel _series;
    try {
      _series = Series.userList.where(
        (SeriesModel series) => series.id == value["series"]
      ).toList()[0];
    }
    catch(e) {
      // There was no log
      continue;
    }


    ExerciseModel _exercise;
    if(value["exercise"] != null) /* Exercise related log */ {
      _exercise = _series.seriesExercises.where(
        (Map<ExerciseModel, dynamic> exerciseMap) => exerciseMap.keys.toList()[0].name == value["exercise"] 
      ).toList()[0].keys.toList()[0];
    }
    else {
      _exercise = null;
    }

    double _completenessPercentage;
    String status = value["status"];
    if(status == "series_start" || 
      status == "series_end" ||
      status == "exercise_start" ||
      status == "exercise_end") {
      _completenessPercentage = null;
    } 
    else if(status.startsWith("exercise_completeness_")) {
      _completenessPercentage = double.parse(status.replaceAll("exercise_completeness_", ""));
    }
    ExerciseCompletenessModel exerciseCompleteness = ExerciseCompletenessModel(
      timestamp: value["timestamp"],
      series: _series,
      exercise: _exercise,
      completenessPercentage: _completenessPercentage,
      status: status,
    );
    ExercisesCompleteness.appendExerciseCompleteness(exerciseCompleteness);
  }
  ExercisesCompleteness.isLoadingExercisesCompleteness = false;
}

Future<PostgreSQLConnection> openConnection() async {
  return await Future(() async {
    var connection = new PostgreSQLConnection(database_url, port, "postgres", username: "postgres", password: "password");
    await connection.open();
    return connection;
  });
}