import 'package:fitledge/Assets/Postgres.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/globals.dart';
import 'package:postgres/postgres.dart';

// Series logs
Future<void> logSeriesStart(SeriesModel series) async {
  String query = """
  INSERT INTO user_series_accomplishment_log VALUES ('${User.instance.username}', ${series.id}, NULL, current_timestamp, 'series_start')
  """;
  _executeQuery(query);
}

/// Gets the date from last log with value "series_start" 
/// from user_series_accomplishment_log table to populate
/// the "series_end" log and change the user_series_accomplishment
/// value to true
Future<void> logSeriesEnd(SeriesModel series) async {
  String insertOrUpdateSeriesAccomplishmentQuery = """
  WITH last_series_start AS (
    SELECT max(timestamp) as last_series_start
    FROM user_series_accomplishment_log
    WHERE username = '${User.instance.username}' AND 
    series = ${series.id} AND
    status = 'series_start'
  )

  INSERT INTO user_series_accomplishment 
  select '${User.instance.username}', ${series.id}, true, cast(last_series_start.last_series_start as DATE) 
  from last_series_start
  on conflict (username, series, date)
  do update set
  done = true;
  """;

  _executeQuery(insertOrUpdateSeriesAccomplishmentQuery);
  String insertSeriesEndQuery = """
    INSERT INTO user_series_accomplishment_log 
    VALUES ('${User.instance.username}', ${series.id}, NULL, current_timestamp, 'series_end');
  """;
  _executeQuery(insertSeriesEndQuery).then((_) {
    Graphs.generateSeriesCompletionData(); // Update series completion graph
  });
}

// Exercise Logs
Future<void> logExerciseStart(SeriesModel series, ExerciseModel exercise) async {
  String query = """
  INSERT INTO user_series_accomplishment_log VALUES ('${User.instance.username}', ${series.id}, '${exercise.name}', current_timestamp, 'exercise_start')
  """;
  _executeQuery(query);
}

Future<void> logExerciseCompletenessPercentage(SeriesModel series, ExerciseModel exercise, double exercisePercentage) async {
  PostgreSQLConnection conn = await openConnection();
  String checkIfExistingCompletenessQuery = """ 
  SELECT *
  FROM user_series_accomplishment_log
  WHERE username = '${User.instance.username}' and
  series = ${series.id} and
  exercise = '${exercise.name}' and
  status like 'exercise_completeness_%' and
  timestamp::date = now()
  """;

  var resultExistingCompleteness = await conn.query(checkIfExistingCompletenessQuery);

  if(resultExistingCompleteness.length > 0) /* Existing completeness log for this series' exercise accomplishment */ {
    String updateCompletenessQuery = """
    UPDATE user_series_accomplishment_log
    SET status = '${"exercise_completeness_" + exercisePercentage.toString()}'
    WHERE username = '${User.instance.username}' and
    series = ${series.id} and
    exercise = '${exercise.name}' and
    status like 'exercise_completeness_%' and
    timestamp::date = now()
    """;
    await conn.query(updateCompletenessQuery);
  }
  else {
    String insertCompletenessQuery = """
    INSERT INTO user_series_accomplishment_log  VALUES ('${User.instance.username}', ${series.id}, '${exercise.name}', current_timestamp, '${"exercise_completeness_" + exercisePercentage.toString()}')
    """;
    await conn.query(insertCompletenessQuery);
  }

  conn.close();
}

Future<void> logExerciseEnd(SeriesModel series, ExerciseModel exercise) async {
  String query = """
  INSERT INTO user_series_accomplishment_log VALUES ('${User.instance.username}', ${series.id}, '${exercise.name}', current_timestamp, 'exercise_end')
  """;
  _executeQuery(query);
}

//
Future<void> _executeQuery(String query) async {
  PostgreSQLConnection conn = await openConnection();
  await conn.query(query);
  conn.close();
}
