import 'package:flutter/material.dart';

class MyDaySelector extends StatefulWidget {
  static const modeFull = monday | tuesday | wednesday | thursday | friday | saturday | sunday;
  static const modeWorkdays = monday | tuesday | wednesday | thursday | friday | saturday;
  static const sunday = 2;
  static const monday = 4;
  static const tuesday = 8;
  static const wednesday = 16;
  static const thursday = 32;
  static const friday = 64;
  static const saturday = 128;
  final int mode;
  final Color color;
  List<int> value;
  final Function(List<int>) onChange;

  MyDaySelector({Key key, this.mode = modeWorkdays, this.onChange, this.color, this.value}) : super(key: key);

  @override
  DayPickerState createState() {
    return DayPickerState();
  }
}

class DayPickerState extends State<MyDaySelector> {
  var _selectedDays = 1; // no day selected
  final _values = [
    MyDaySelector.sunday,
    MyDaySelector.monday,
    MyDaySelector.tuesday,
    MyDaySelector.wednesday,
    MyDaySelector.thursday,
    MyDaySelector.friday,
    MyDaySelector.saturday,
  ];
  final _shortLabels = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
  final _labels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  // int get selectedDays => _selectedDays == 1 ? null : _selectedDays;
  List<int> get selectedDays => getSelectedDays(_selectedDays);

  bool _isDayAllowed(int day) {
    return widget.mode & day == day;
  }

  @override
  void initState() {
    _selectedDays = getValueAsInt(widget.value) ?? 1;
    super.initState();
  }

  @override
  void didUpdateWidget(MyDaySelector oldWidget) {
    if (_selectedDays != getValueAsInt(widget.value)) {
      _selectedDays = getValueAsInt(widget.value) ?? 1;
    }
    super.didUpdateWidget(oldWidget);
  }

  int getValueAsInt(List<int> values) {
    if (values == []) return null;
    int value = 0;
    if(values.contains(0)) value += 2;
    if(values.contains(1)) value += 4;
    if(values.contains(2)) value += 8;
    if(values.contains(3)) value += 16;
    if(values.contains(4)) value += 32;
    if(values.contains(5)) value += 64;
    if(values.contains(6)) value += 128;
    return value;
  }

  List<int> getSelectedDays(int selectedDaysValue) {
    List<int> selectedDays = [];
    if (selectedDaysValue & MyDaySelector.sunday == MyDaySelector.sunday) {
      selectedDays.add(0);
    }
    if (selectedDaysValue & MyDaySelector.monday == MyDaySelector.monday) {
      selectedDays.add(1);
    }
    if (selectedDaysValue & MyDaySelector.tuesday == MyDaySelector.tuesday) {
      selectedDays.add(2);
    }
    if (selectedDaysValue & MyDaySelector.wednesday == MyDaySelector.wednesday) {
      selectedDays.add(3);
    }
    if (selectedDaysValue & MyDaySelector.thursday == MyDaySelector.thursday) {
      selectedDays.add(4);
    }
    if (selectedDaysValue & MyDaySelector.friday == MyDaySelector.friday) {
      selectedDays.add(5);
    }
    if (selectedDaysValue & MyDaySelector.saturday == MyDaySelector.saturday) {
      selectedDays.add(6);
    }
    return selectedDays;
  }

  @override
  Widget build(BuildContext context) {
    final days = <Widget>[];

    Widget _getDay(int index) {
      return Expanded(
            child: _Day(
            color: widget.color,
            label: _shortLabels[index],
            selected: _selectedDays & _values[index] == _values[index],
            value: _values[index],
            onTap: (value) {
              setState(() {
                if (_selectedDays & value == value) {
                  _selectedDays &= ~value;
                } else {
                  _selectedDays |= value;
                }
                if (widget.onChange != null) {
                  widget.onChange(getSelectedDays(_selectedDays));
                }
              });
            },
          ),
      );
    }

    if (_isDayAllowed(MyDaySelector.monday)) {
      days.add(_getDay(0));
    }
    if (_isDayAllowed(MyDaySelector.tuesday)) {
      days.add(_getDay(1));
    }
    if (_isDayAllowed(MyDaySelector.wednesday)) {
      days.add(_getDay(2));
    }
    if (_isDayAllowed(MyDaySelector.thursday)) {
      days.add(_getDay(3));
    }
    if (_isDayAllowed(MyDaySelector.friday)) {
      days.add(_getDay(4));
    }
    if (_isDayAllowed(MyDaySelector.saturday)) {
      days.add(_getDay(5));
    }
    if (_isDayAllowed(MyDaySelector.sunday)) {
      days.add(_getDay(6));
    }

    return Row(
      children: days,
    );
  }
}

class _Day extends StatefulWidget {
  final Color color;
  final bool selected;
  final String label;
  final int value;
  final Function(int) onTap;
  
  const _Day({Key key, this.color, this.label, this.selected=false, this.value, this.onTap}) : super(key: key);

  @override
  _DayState createState() {
    return _DayState(this.color, this.label, this.selected, this.value, this.onTap);
  }

}


class _DayState extends State<_Day> with SingleTickerProviderStateMixin {
  Color color;
  bool selected;
  String label;
  int value;
  Function(int) onTap;
  AnimationController animationController;
  Animation<Offset> animation;
  _DayState(this.color, this.label, this.selected, this.value, this.onTap);

  void initState() {
    this.animationController = new AnimationController(vsync: this, duration: Duration(milliseconds: 100));
    this.animation = Tween<Offset>(
      begin: Offset.zero,
      end: const Offset(0, -0.1)
    ).animate(CurvedAnimation(
      parent:animationController,
      curve: Curves.linear
    ));
    animation.addStatusListener((status) {
      if(status == AnimationStatus.completed || status == AnimationStatus.dismissed) {
        animationController.stop();
      }
    });

    if (this.selected) {
      this.animationController.value = 1;
    }
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonThemeData buttonTheme = ButtonTheme.of(context);
    var borderColor = Colors.black;
    if (ThemeData.estimateBrightnessForColor(color ?? buttonTheme.colorScheme.background) == Brightness.dark) {
      borderColor = Theme.of(context).accentColor;
    }
    return SlideTransition(
      position: animation,
      child: RawMaterialButton(
        onPressed: () {
          if (!selected) {
            animationController.forward();
          }
          else {
            animationController.reverse();
          }
          onTap(value);
          setState(() {
            this.selected = !this.selected;
          });
        },
        elevation: selected ? 4 : 2,
        constraints: BoxConstraints(minWidth: 40, minHeight: 40),
        // fillColor: color ?? buttonTheme.colorScheme.background,
        fillColor: selected ? Colors.deepOrange[300] : Colors.deepOrange,
        textStyle: Theme.of(context).textTheme.button,
        child: Text(label, style: TextStyle(color: borderColor)),
        shape: CircleBorder(side: selected ? BorderSide(color: borderColor, width: 1) : BorderSide.none),
      ),
    );
  }
}
