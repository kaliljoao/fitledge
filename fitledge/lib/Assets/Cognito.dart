import 'dart:convert';
import 'dart:io';

import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:amazon_cognito_identity_dart/sig_v4.dart';
import 'package:fitledge/Store/register.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:path/path.dart' as path;
import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';

Future registerUser(RegisterData registerData) async {
  var userPool = await _getUserPool();
  final userAttributes = [
    AttributeArg(name: "email", value: registerData.email)
  ];

  return await Future(() async {
    CognitoUserPoolData userPoolData = await userPool.signUp(registerData.username, 
      registerData.password, 
      userAttributes: userAttributes);
    User.instance.username = registerData.username;
    return userPoolData;
  });
}

Future waitConfirmation(username,password) async {
  bool userConfirmed = false;
  return await Future(() async {
    while(!userConfirmed) {
      await Future.delayed(new Duration(seconds: 3), () {
        loginUser(username, password)
        .then((_) {userConfirmed = true;})
        .catchError((_) {});
      });
    }
  });
}

Future loginUser(username,password) async {
  var userPool = await _getUserPool();
  CognitoUser cognitoUser = new CognitoUser(username, userPool);
  CognitoUserSession userSession;

  final authDetails = new AuthenticationDetails(
    username: username,
    password: password
  );
  return await Future(() async {
    userSession = await cognitoUser.authenticateUser(authDetails);
    User.instance.username = username;
    User.instance.password = password;
    User.instance.userSession = userSession;

    return userSession;
  });
}

Future<bool> checkUserExists(String username) async {
  bool userExists;
  var userPool = await _getUserPool();
  CognitoUser cognitoUser = new CognitoUser(username, userPool);

  final authDetails = new AuthenticationDetails(
    username: username,
    password: "foo"
  );
  return await Future(() async {
    await cognitoUser.authenticateUser(authDetails)
    .then((_) {})
    .catchError((exc) {
      if(exc.message == "User does not exist.") {
        userExists = false;
      }
      else {
        userExists = true;
      }
    });
    return userExists;
  });
}

void resendConfirmationEmail(username) async {
  var userPool = await _getUserPool();
  CognitoUser cognitoUser = new CognitoUser(username, userPool);
  cognitoUser.resendConfirmationCode();
}

Future<bool> signOut() async { 
  CognitoUserPool userPool = await _getUserPool();
  CognitoUser user = await userPool.getCurrentUser();
  try{
    await user.signOut();
    Series.resetSeries();
    Exercises.resetExercises();
    ExercisesCompleteness.resetExercisesCompleteness();
    return true;
  } catch (e){
    return false;
  }
}

Future<CognitoUserPool> _getUserPool() async {
  String userPoolId;
  String clientId;
  return await Future(() async {
    userPoolId = await rootBundle.loadString("assets/user_pool_id.txt");
    clientId = await rootBundle.loadString("assets/client_id.txt");
    return new CognitoUserPool(userPoolId, clientId);
  });
}

Future<void> uploadProfilePicture(String filePath) async {
  // CognitoUserPool pool = await _getUserPool();
  // Map<String, dynamic> params = {
  //   "Bucket":"fitledge",
  //   "Key": "profile_pics/test.jpg",
  //   "data": File(filePath).readAsBytesSync()
  // };
  // pool.client.request("PutObject", params, service: "s3");
  String _identityPoolId = await rootBundle.loadString("assets/cognito_identity.txt");
  final _userPool = await _getUserPool();

  final _cognitoUser = CognitoUser(User.instance.username, _userPool);
  final authDetails =
      AuthenticationDetails(username: User.instance.username, password: User.instance.password);

  CognitoUserSession userSession;
  try {
    userSession = await _cognitoUser.authenticateUser(authDetails);
  } catch (e) {
    print(e);
  }

  final _credentials = CognitoCredentials(_identityPoolId, _userPool);
  await _credentials.getAwsCredentials(userSession.getIdToken().getJwtToken());

  const _region = 'us-east-2';
  const _s3Endpoint =
      'https://fitledge.s3.us-east-2.amazonaws.com';

  final file = File(filePath);

  final stream = http.ByteStream(DelegatingStream.typed(file.openRead()));
  final length = await file.length();

  final uri = Uri.parse(_s3Endpoint);
  final req = http.MultipartRequest("POST", uri);
  final multipartFile = http.MultipartFile('file', stream, length,
      filename: path.basename(file.path));

  final policy = _Policy.fromS3PresignedPost(
      'profile_pics/${User.instance.username}.jpg',
      'fitledge',
      15,
      _credentials.accessKeyId,
      length,
      _credentials.sessionToken,
      region: _region);
  final key = SigV4.calculateSigningKey(
      _credentials.secretAccessKey, policy.datetime, _region, 's3');
  final signature = SigV4.calculateSignature(key, policy.encode());

  req.files.add(multipartFile);
  req.fields['key'] = policy.key;
  req.fields['acl'] = 'public-read';
  req.fields['X-Amz-Credential'] = policy.credential;
  req.fields['X-Amz-Algorithm'] = 'AWS4-HMAC-SHA256';
  req.fields['X-Amz-Date'] = policy.datetime;
  req.fields['Policy'] = policy.encode();
  req.fields['X-Amz-Signature'] = signature;
  req.fields['x-amz-security-token'] = _credentials.sessionToken;

  try {
    final res = await req.send();
    await for (var value in res.stream.transform(utf8.decoder)) {
      print(value);
    }
  } catch (e) {
    print(e.toString());
  }
}


class _Policy {
  String expiration;
  String region;
  String bucket;
  String key;
  String credential;
  String datetime;
  String sessionToken;
  int maxFileSize;

  _Policy(this.key, this.bucket, this.datetime, this.expiration, this.credential,
      this.maxFileSize, this.sessionToken,
      {this.region = 'us-east-2'});

  factory _Policy.fromS3PresignedPost(
    String key,
    String bucket,
    int expiryMinutes,
    String accessKeyId,
    int maxFileSize,
    String sessionToken, {
    String region,
  }) {
    final datetime = SigV4.generateDatetime();
    final expiration = (DateTime.now())
        .add(Duration(minutes: expiryMinutes))
        .toUtc()
        .toString()
        .split(' ')
        .join('T');
    final cred =
        '$accessKeyId/${SigV4.buildCredentialScope(datetime, region, 's3')}';
    final p = _Policy(
        key, bucket, datetime, expiration, cred, maxFileSize, sessionToken,
        region: region);
    return p;
  }

  String encode() {
    final bytes = utf8.encode(toString());
    return base64.encode(bytes);
  }

  @override
  String toString() {
    return '''
{ "expiration": "${this.expiration}",
  "conditions": [
    {"bucket": "${this.bucket}"},
    ["starts-with", "\$key", "${this.key}"],
    {"acl": "public-read"},
    ["content-length-range", 1, ${this.maxFileSize}],
    {"x-amz-credential": "${this.credential}"},
    {"x-amz-algorithm": "AWS4-HMAC-SHA256"},
    {"x-amz-date": "${this.datetime}" },
    {"x-amz-security-token": "${this.sessionToken}" }
  ]
}
''';
  }
}
