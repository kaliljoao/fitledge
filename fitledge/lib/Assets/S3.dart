import 'dart:convert';
import 'package:amazon_s3_cognito/amazon_s3_cognito.dart';
import 'package:amazon_s3_cognito/aws_region.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_aws_s3_client/flutter_aws_s3_client.dart';

Future<Map<String, Image>> getSeriesTypesImages() async {
  AwsS3Client client = await getS3Client();
  ListBucketResult bucketResults = await client.listObjects(prefix: "series/images");
  List<dynamic> keysList = json.decode(bucketResults.toJson())["Contents"];
  Map<String, Image> imageMap = {};
  for(final key in keysList.sublist(1)) {
    var res = await client.getObject(key["Key"]);
    String seriesType = key["Key"].split("/").last.split(".")[0];
    imageMap[seriesType] = Image.memory(res.bodyBytes);
  }
  Series.defaultSeriesImages = imageMap;
  Series.isLoadingDefaultSeriesImages = false;
}

Future<Image> getExerciseThumbnailImage(ExerciseModel exercise, AwsS3Client client) async {
  Image image;
  try {
    var res = await client.getObject("series/${exercise.type}/${exercise.name}/thumbnail.jpg");
    image =  Image.memory(res.bodyBytes);
  }
  catch(e) {
    image = Image.asset("assets/Error.jpg");
  }
  return image;
}

Future<ImageProvider> getProfilePicture() async {
  AwsS3Client client = await getS3Client();
  var res = await client.getObject("profile_pics/${User.instance.username}.jpg");
  if(res.statusCode == 200) {
    return MemoryImage(res.bodyBytes);
  }
  else {
    return AssetImage("assets/images/avatarImage.png");
  }
}

Future <AwsS3Client> getS3Client() async {
  const region = "us-east-2";
  const bucketId = "fitledge";
  
  final AwsS3Client s3Client = AwsS3Client(
    region: region,
    host: "s3.$region.amazonaws.com",
    bucketId: bucketId,
    accessKey: await rootBundle.loadString("assets/s3_access_key.txt"),
    secretKey: await rootBundle.loadString("assets/s3_secret_key.txt")
  );
  return s3Client;
}