library fitledge.globals;
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/graphInfo.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/user.dart';
import 'package:fitledge/Widgets/Charts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:charts_flutter/flutter.dart' as chart;
import 'dart:async';

class User {
  User._privateConstructor();
  static final UserModel _privateUser = UserModel();
  static UserModel get instance => _privateUser;
  static void fetchUserData() {
    _privateUser.fetchUserData();
  }
}

class Exercises {
  Exercises._privateConstructor();
  static final ExercisesList _privateExercisesList = ExercisesList();
  
  static List<ExerciseModel> get list => _privateExercisesList.list;
  static List<String> get typeList => _privateExercisesList.typeList;
  static bool get isLoadingExercises => _privateExercisesList.isLoadingExercises;
  static set isLoadingExercises(bool value) {_privateExercisesList.isLoadingExercises = value;}
  static void append(ExerciseModel exercise) {_privateExercisesList.addExercise(exercise);}
  static void resetExercises() {_privateExercisesList.list = new List<ExerciseModel>();} 
}

class Series {
  Series._privateConstructor();
  static final SeriesList _privateSeriesList = SeriesList();
  
  static Map<String, Image> get defaultSeriesImages => _privateSeriesList.defaultSeriesImages;
  static set defaultSeriesImages(Map<String,Image> value) { _privateSeriesList.defaultSeriesImages = value; }
  static bool get isLoadingDefaultSeriesImages => _privateSeriesList.isLoadingDefaultSeriesImages;
  static set isLoadingDefaultSeriesImages(bool value) {_privateSeriesList.isLoadingDefaultSeriesImages = value;}
  

  static List<SeriesModel> get list => _privateSeriesList.list; // Non-user made series
  static List<SeriesModel> get userList => _privateSeriesList.userList; // All Series this user has added for himself
  static List<String> get typeList => _privateSeriesList.typeList;
  
  static bool get isLoadingSeries => _privateSeriesList.isLoadingSeries;
  static set isLoadingSeries(bool value) {_privateSeriesList.isLoadingSeries = value;}
  static void append(SeriesModel series) {_privateSeriesList.addSeries(series);}
  
  static bool get isLoadingUserSeries => _privateSeriesList.isLoadingUserSeries;
  static set isLoadingUserSeries(bool value) {_privateSeriesList.isLoadingUserSeries = value;}
  static void appendUserSeries(SeriesModel series) {_privateSeriesList.addUserSeries(series);}
  static void removeUserSeries(SeriesModel series) {_privateSeriesList.removeUserSeries(series);}

  static void resetSeries() {
    _privateSeriesList.list = new List<SeriesModel>();
    _privateSeriesList.userList = new List<SeriesModel>();
  } 
}

class Local {
  static final Local _locale = Local._internal();
  factory Local() => _locale;
  Local._internal(); 

  static Locale local;

  static Locale getLocal() { return local; }
  static void setLocal(Locale locale) { local = locale; }
}

class Battery {
  static const platform = const MethodChannel('samples.flutter.dev/battery');
  
  static Future<int> get getBattery => _getBatteryLevel();
  static Future<int> _getBatteryLevel() async {
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      return result;
    } on PlatformException catch (e) {
      return -1;
    }
  }
}

class Graphs {
  Graphs._privateConstructor();
  static final GraphInfo _privateGraphInfo = GraphInfo();
  
  static DateTime get dataBeginDate => _privateGraphInfo.dataBeginDate;
  static DateTime get dataEndDate => _privateGraphInfo.dataEndDate;
  static set dataBeginDate(DateTime value) {_privateGraphInfo.dataBeginDate = value;}
  static set dataEndDate(DateTime value) {_privateGraphInfo.dataEndDate = value;}

  static bool get isLoadingSeriesCompletionChartData => _privateGraphInfo.isLoadingSeriesCompletionChartData;
  static bool get isLoadingTotalTimeCardData => _privateGraphInfo.isLoadingTotalTimeCardData;
  static bool get isLoadingNumberOfDoneSeries => _privateGraphInfo.isLoadingNumberOfDoneSeries;
   
  static List<chart.Series<SeriesCompletionData, DateTime>> get seriesCompletionSeries => _privateGraphInfo.seriesCompletionSeries;
  static String get totalTimeSpentData => _privateGraphInfo.totalTimeSpentData;
  static int get numberOfDoneSeries => _privateGraphInfo.numberOfDoneSeries;

  static void generateSeriesCompletionData() {_privateGraphInfo.generateSeriesCompletionData();}
  static void updateTimeIntervalDependantCharts() {_privateGraphInfo.updateTimeIntervalDependantCharts();}
}

class ExercisesCompleteness {
  ExercisesCompleteness._privateConstructor();
  static final ExerciseCompletenessList _privateExercisesCompletenessList = ExerciseCompletenessList();
  
  static List<ExerciseCompletenessModel> get exerciseCompletenessList => _privateExercisesCompletenessList.execisesCompletenessList;
  static List<SeriesModel> get completedSeriesList => _privateExercisesCompletenessList.completedSeries;
  static bool get isLoadingExercisesCompleteness => _privateExercisesCompletenessList.isLoadingExercisesCompleteness;
  static set isLoadingExercisesCompleteness(bool value) {_privateExercisesCompletenessList.isLoadingExercisesCompleteness = value;}
  static void appendExerciseCompleteness(ExerciseCompletenessModel value) {_privateExercisesCompletenessList.execisesCompletenessList.add(value);}
  static void appendCompletedSeries(SeriesModel value) {_privateExercisesCompletenessList.completedSeries.add(value);}
  static void resetExercisesCompleteness() {
    _privateExercisesCompletenessList.execisesCompletenessList = new List<ExerciseCompletenessModel>();
    _privateExercisesCompletenessList.completedSeries = new List<SeriesModel>();
  } 
  static double getPercentage() {
    double percent = 0;
    int completeds = 0;
    int totalExercises = exerciseCompletenessList.length;

    exerciseCompletenessList.forEach((element) { 
      if (element.completenessPercentage == 100) {
        completeds++;
      }
    });

    return completeds/totalExercises * 100;
  }
}
