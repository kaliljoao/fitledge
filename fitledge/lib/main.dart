import 'package:fitledge/UI/LoginUI.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'package:fitledge/UI/ProfileUI.dart';
import 'package:fitledge/UI/RegisterUI.dart';
import 'package:fitledge/UI/InitialUI.dart';
import 'package:fitledge/UI/SeriesUserUI.dart';
import 'package:flutter/services.dart' ;
import 'package:flutter_localizations/flutter_localizations.dart';

import 'Assets/Localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) { 
    
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      supportedLocales: [Locale('en', 'US'), Locale("es"), Locale("pt", "BR")],
      localizationsDelegates: [
        Localization.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      localeResolutionCallback: (locale, supportedLocales) {
        for( var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode && supportedLocale.countryCode == locale.countryCode) {
            Local.local;
            Local.setLocal(supportedLocale);
            print(Local.getLocal());
            return supportedLocale;
          }
        }
        
        return supportedLocales.first;
      },
      debugShowCheckedModeBanner: false,
      title: 'FitLedge',
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
      darkTheme: ThemeData.dark(),
      home: MyLoginPage(),
      routes: {
        '/home' : (_) => new MainUI(),
        '/register/1': (_) => new MyRegisterScreen_1(),
        '/register/2': (_) => new MyRegisterScreen_2(),
        '/profile': (_) => new Profile(),
        '/userSeries': (_) => new SeriesUserUI(),
      },
    );
  }
}

class MyLoginPage extends StatefulWidget {
  MyLoginPage({Key key,}) : super(key: key);
  
  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black87,
      ),
      backgroundColor: Colors.black87,
      body: LoginUI(),
    );
  }
}

class MyRegisterScreen_1 extends StatefulWidget {
  MyRegisterScreen_1({Key key,}) : super(key: key);
  
  @override
  _MyRegisterPageState_1 createState() => _MyRegisterPageState_1();
}

class _MyRegisterPageState_1 extends State<MyRegisterScreen_1> {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black87,
      ),
      backgroundColor: Colors.black87,
      body: RegisterUI_1(),
    );
  }
}

class MyRegisterScreen_2 extends StatefulWidget {
  MyRegisterScreen_2({Key key,}) : super(key: key);
  
  @override
  _MyRegisterPageState_2 createState() => _MyRegisterPageState_2();
}

class _MyRegisterPageState_2 extends State<MyRegisterScreen_2> {
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.black87,
      ),
      backgroundColor: Colors.black87,
      body: RegisterUI_2(),
    );
  }
}

