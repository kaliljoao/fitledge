import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/UI/SeriesDetailsUI.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

Color DELETE_COLOR = Colors.red[800];

Widget SeriesListView(BuildContext context, List<SeriesModel> series, bool isPractice,{Function(SeriesModel) onCardPressed, bool isEdit, Function(SeriesModel) onDeletePressed}) {
  double _getSeriesCompletenessPercentage(SeriesModel series) {
    double _completion = 0;
    int exercisesCount = series.seriesExercises.length;
    List<double> exercisesCompleteness = [];
  
    series.seriesExercises.forEach((Map<ExerciseModel, dynamic> exerciseMap) {
      ExerciseModel exercise = exerciseMap.keys.toList()[0];
      if(statusExists(seriesId: series.id, exerciseName: exercise.name, status: "exercise_end")){
        exercisesCompleteness.add(1.0);
      }
      else {
        exercisesCompleteness.add(
          getExerciseCompleteness(series: series, exercise: exercise)
        );
      }
    });

    for(double exerciseCompleteness in exercisesCompleteness) {
      _completion += exerciseCompleteness / exercisesCount;
    }
    return _completion;
  }
  isEdit ??= false;
  return ListView(
    children: series.map((series) {
      return Stack(
        children:  [
            GestureDetector(
            onTap: () {
              if(onCardPressed == null)
                Navigator.push(context, MaterialPageRoute(builder: (_) => SeriesDetailUI(series, false)));
              else if(!isEdit)
                onCardPressed(series);
            },
            child: Card(
              margin: EdgeInsets.symmetric(horizontal: 5, vertical:4),
              color: Colors.black54,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
              child: Container(
                  // height: 90,
                  padding: EdgeInsets.all(8.0),
                  alignment: Alignment(0,0),
                  child: ListTile(
                    // leading: FlutterLogo(),
                    trailing: 
                    !isEdit ? 
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget> [
                        Icon(
                          Icons.arrow_forward_ios,
                          color: Colors.deepOrange
                        )
                      ]
                    )
                    :
                    SizedBox(width: 0,height: 0,),
                    title: Row (
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Expanded(
                        flex: 5,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              series.name,
                              style: TextStyle(
                                color: Colors.white, 
                                fontSize: 20
                              ),
                            ),
                            Text(
                              series.daysStringFormat.join("/"),
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.grey
                              ),
                              overflow: TextOverflow.fade,
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        child: isPractice ? ExercisesCompleteness.completedSeriesList.contains(series) ? 
                        CircularPercentIndicator(
                          radius: 35.0,
                          lineWidth: 3.0,
                          percent: 1,
                          progressColor: Colors.green[800],
                        )
                        :
                        CircularPercentIndicator(
                          radius: 35.0,
                          lineWidth: 3.0,
                          percent: _getSeriesCompletenessPercentage(series),
                          // center: new Text("60%"),
                          progressColor: Colors.yellow,
                        )
                        :
                        Container()
                      ) 
                    ]
                  ),     
                )
              )
            )
          ),
          isEdit ? 
            Positioned(
              right: 0,
              top: 0,
              child: GestureDetector(
                onTap: () => onDeletePressed(series),
                child: Container(
                  height: 34,
                  width: 34,
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: DELETE_COLOR,
                      width: 3
                    ),
                    borderRadius: BorderRadius.circular(50)
                  ),
                  child: Icon(
                    Icons.delete_forever,
                    color: DELETE_COLOR,
                    size: 24,
                  ),
                ),
              ),
            )
            :
            Container()
        ],
      );
    }).toList()
  );

} 