import 'package:fitledge/UI/SeriesUI.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class FitLedgeCustomCard extends StatelessWidget{

  String exerciseType;

  FitLedgeCustomCard(this.exerciseType);

  Image getImage() {
    if(Series.defaultSeriesImages.keys.contains(this.exerciseType)) {
      return Series.defaultSeriesImages[this.exerciseType];
    }
    else {
      return Series.defaultSeriesImages["Error"];
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => SeriesList(exerciseType)));
      },
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child:Stack(
          children: <Widget>[
            Series.defaultSeriesImages[this.exerciseType],
            Positioned(
              bottom: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                decoration: BoxDecoration(
                    color: Colors.deepOrange[600],
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      bottomRight: Radius.circular(8),
                    )
                ),
                child: Text(
                  exerciseType,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20
                  ),
                ),
              ),
            )
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(7.0),
        ),
        elevation: 5,
        margin: EdgeInsets.all(10),
      )
    ); 
  }
}