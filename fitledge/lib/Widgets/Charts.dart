
import 'dart:io';

import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as chart;
import 'package:flutter_mobx/flutter_mobx.dart';

class SeriesCompletionChart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SeriesCompletionChartState();
}

class SeriesCompletionChartState extends State<SeriesCompletionChart> {
  Widget _getChart() /* This is due to a bug: TODO FIX https://github.com/flutter/flutter/issues/31778 */{
    return FutureBuilder(
      initialData: 0,
      future: Future.delayed(Duration(milliseconds:500), () => 1),
      builder: (context, snapshot) {
        if(snapshot.data == 1) {
          return chart.TimeSeriesChart(
            Graphs.seriesCompletionSeries,
            // animate: Graphs.shouldAnimateSeriesCompletion(),
            animate: true,
            animationDuration: Duration(milliseconds: 300),
            defaultRenderer: new chart.LineRendererConfig(
              includeArea: true,
              stacked:true,
            ),
            domainAxis: chart.DateTimeAxisSpec(
              renderSpec: chart.GridlineRendererSpec(
                labelStyle: chart.TextStyleSpec(
                  color: chart.ColorUtil.fromDartColor(Colors.deepOrange)
                ),
                lineStyle: chart.LineStyleSpec(
                  color: chart.ColorUtil.fromDartColor(Colors.transparent)
                ),
              )
            ),
            primaryMeasureAxis: chart.NumericAxisSpec(
              renderSpec: chart.GridlineRendererSpec(
                labelStyle: chart.TextStyleSpec(
                  color: chart.ColorUtil.fromDartColor(Colors.deepOrange)
                ),
                // lineStyle: chart.LineStyleSpec(
                //   color: chart.ColorUtil.fromDartColor(Colors.deepOrange)
                // ),
              )
            ),
            behaviors: [
              new chart.ChartTitle(
                "Date",
                behaviorPosition: chart.BehaviorPosition.bottom,
                titleOutsideJustification: chart.OutsideJustification.middleDrawArea,  
                titleStyleSpec: chart.TextStyleSpec(fontSize: 13, color: chart.ColorUtil.fromDartColor(Colors.deepOrange)),
              ),
              new chart.ChartTitle(
                "Completion (%)",
                behaviorPosition: chart.BehaviorPosition.start,
                titleOutsideJustification: chart.OutsideJustification.middleDrawArea,
                titleStyleSpec: chart.TextStyleSpec(fontSize: 13, color: chart.ColorUtil.fromDartColor(Colors.deepOrange)),
              ),
            ]
          );
        }
        return Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation(Colors.deepOrange),));
      } 
    );
  }


  Widget build(BuildContext context) {
    return Container(
      height: 300,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(5.0)
        ),
        color: Color(0xff151515)
      ),
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(top:10.0),
              child: Text(
                  "Series completion",
                  style: TextStyle(color: Colors.deepOrange, fontSize: 14, fontFamily: 'Georgia', fontWeight: FontWeight.w700),
                ),
            ),
          ),
          Observer(
            builder: (context){
              if(!Graphs.isLoadingSeriesCompletionChartData) {
              return Padding(
                padding: const EdgeInsets.only(top:25.0),
                child: _getChart()              
                );
              }
              else {
                return Center(
                  child: Container(
                    height: 40,
                    width: 40,
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange)
                    )
                  ),
                ); 
              }
            }
          ),
        ],
      ),
    );
  }
}

class ExerciseTypeChart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ExerciseTypeChartState();
}

class ExerciseTypeChartState extends State<ExerciseTypeChart> {
  List<chart.Series<ExerciseTypeData, String>> exerciseTypeSeries;
  List<Color> colors = [Colors.deepOrange[900],Colors.indigo[400],Colors.lightGreen[900],Colors.deepPurple[900],];
  @override
  void initState() {
    this.exerciseTypeSeries = List<chart.Series<ExerciseTypeData, String>>();
  }

  List<chart.Series<ExerciseTypeData, String>> _getSeriesTypeData() {

    Map<String, int> seriesTypeCount = {};
    List<ExerciseTypeData> exerciseTypeDataCollection = [];
    List<chart.Series<ExerciseTypeData, String>> exerciseTypeSeriesData = [];
    int count = 0;
    for(SeriesModel series in Series.userList) {
      for(Map<ExerciseModel, dynamic> exerciseMap in series.seriesExercises) {
        ExerciseModel exercise = exerciseMap.keys.toList()[0];
        String type = exercise.type;
        if(!seriesTypeCount.containsKey(type)) seriesTypeCount[exercise.type] = 1;
        else seriesTypeCount[exercise.type] += 1;
        count += 1;
      }
    }
    int index = 0;
    seriesTypeCount.forEach((type, typeCount) {
      exerciseTypeDataCollection.add(
        new ExerciseTypeData(type, (typeCount/count)*100, colors[index++])
      );
    });

    exerciseTypeSeriesData.add(
      chart.Series(
        id: "Exercise type percentage",
        data: exerciseTypeDataCollection,
        domainFn: (ExerciseTypeData exerciseTypeData, _) => exerciseTypeData.exerciseType,
        measureFn: (ExerciseTypeData exerciseTypeData, _) => exerciseTypeData.exerciseValue,
        colorFn: (ExerciseTypeData exerciseTypeData, _) => chart.ColorUtil.fromDartColor(exerciseTypeData.color),
        labelAccessorFn: (ExerciseTypeData exerciseTypeRow, _) => '${exerciseTypeRow.exerciseValue.toStringAsFixed(0)}%'
      )
    );
    return exerciseTypeSeriesData;
  }

  Widget build(BuildContext context) {
    return Expanded(
      flex: 20,
      child: Container(
        height: 250,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(5.0)
          ),
          color: Color(0xff151515)
        ),
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Text(
                  "Series types",
                  style: TextStyle(
                    color: Colors.deepOrange,
                    fontSize: 14, 
                    fontFamily: 'Georgia', 
                    fontWeight: FontWeight.w700
                  ),
                ),
              ),
            ),
            SizedBox(height: 10),
            Observer(
              builder: (context) {
                if(!Series.isLoadingUserSeries && !Exercises.isLoadingExercises) /* This chart depends on the user series and its' exercises */ {
                  this.exerciseTypeSeries = _getSeriesTypeData();
                  return Padding(
                    padding: const EdgeInsets.only(top:30.0),
                    child: chart.PieChart(
                      exerciseTypeSeries,
                      animate: true,
                      animationDuration: Duration(milliseconds: 300),
                      defaultRenderer: new _MyArcRendererConfig(),
                      behaviors: [
                        new chart.DatumLegend(
                          outsideJustification: chart.OutsideJustification.startDrawArea,
                          horizontalFirst: false,
                          desiredMaxRows: 2,
                          cellPadding: EdgeInsets.only(right:4.0, bottom:4.0),
                          entryTextStyle: chart.TextStyleSpec(
                            color: chart.ColorUtil.fromDartColor(Colors.deepOrange),
                            fontFamily: 'Georgia',
                            fontSize: 14,
                          )
                        )
                      ],
                    ),
                  );
                }
                else {
                  return Center(
                    child: Container(
                      height: 40,
                      width: 40,
                      child: CircularProgressIndicator(
                        valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange)
                      )
                    ),
                  ); 
                }
              }
            ),
          ],
        ),
      ),
    );
  }
}

class CaloriesBurntCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CaloriesBurntCardState();
}

class CaloriesBurntCardState extends State<CaloriesBurntCard> {
  double totalBurntCalories;
  void initState() {
    getTotalCaloriesMock();
  }

  void getTotalCaloriesMock() {
    totalBurntCalories = 55.1;
  }

  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: Color(0xff151515)
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 8),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                "Calories burnt",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w600,
                  color: Colors.deepOrange
                ),
              ),
              SizedBox(height: 5,),
              Text(
                "${totalBurntCalories} kcal",
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w800,
                  color: Colors.deepOrange[800]
                ),
              ),
            ],
          ),
        ),
      )
    );
  }
}

class TimeSpentCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => TimeSpentCardState();
}

class TimeSpentCardState extends State<TimeSpentCard> {
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: Color(0xff151515)
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 8),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                  "Total exercise time",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Colors.deepOrange
                  ),
                ),
              SizedBox(height: 5,),
              Observer(
                builder: (context) {
                  if(!Graphs.isLoadingTotalTimeCardData) {
                    return Text(
                      "${Graphs.totalTimeSpentData}",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                        color: Colors.deepOrange[800]
                      ),
                    );
                  }
                  else {
                    return CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange)
                    );
                  } 
                } 
              ),
            ],
          ),
        ),
      )
    );
  }
}

class NumberOfSeriesDoneCard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => NumberOfSeriesDoneCardState();
}

class NumberOfSeriesDoneCardState extends State<NumberOfSeriesDoneCard> {
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
        color: Color(0xff151515)
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 5, bottom: 8),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                  "Completed series",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: Colors.deepOrange
                  ),
                ),
              SizedBox(height: 5,),
              Observer(
                builder: (context) {
                  if(!Graphs.isLoadingNumberOfDoneSeries) {
                    return Text(
                      "${Graphs.numberOfDoneSeries}",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w800,
                        color: Colors.deepOrange[800]
                      ),
                    );
                  }
                  else {
                    return CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(Colors.deepOrange)
                    );
                  } 
                } 
              ),
            ],
          ),
        ),
      )
    );
  }
}

class SeriesCompletionData {
  // Map<String, int> date;
  final DateTime date;
  final double completionPercentage;

  SeriesCompletionData(this.date, this.completionPercentage);
}

class ExerciseTypeData {
  String exerciseType;
  double exerciseValue;
  Color color;

  ExerciseTypeData(this.exerciseType, this.exerciseValue, this.color);
}

class _MyArcRendererConfig extends chart.ArcRendererConfig {
  var stroke;
  var arcWidth;
  var arcRendererDecorators;

  _MyArcRendererConfig() : this.stroke = chart.StyleFactory.style.transparent,
                          this.arcWidth = 35,
                          this.arcRendererDecorators= [
                            new chart.ArcLabelDecorator(
                              insideLabelStyleSpec: chart.TextStyleSpec(fontSize: 12, color: chart.ColorUtil.fromDartColor(Colors.white)),
                              outsideLabelStyleSpec: chart.TextStyleSpec(fontSize: 12, color: chart.ColorUtil.fromDartColor(Colors.deepOrange)),
                              labelPosition: chart.ArcLabelPosition.inside,
                            )
                          ]
  ;
}