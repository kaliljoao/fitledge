import 'package:fitledge/Assets/Localizations.dart';
import 'package:fitledge/Assets/Logging.dart';
import 'package:fitledge/Store/exerciseCompletion.dart';
import 'package:fitledge/Store/exercises.dart';
import 'package:fitledge/Store/series.dart';
import 'package:fitledge/Store/user.dart';
import 'package:fitledge/globals.dart';
import 'package:flutter/material.dart';
import 'dart:math' as Math;

class ExerciseCircularIndicator extends StatefulWidget {
  ExerciseCircularIndicator({Key key, this.exercise, this.series, this.listRepetitions}) : super(key: key);

  final ExerciseModel exercise;
  final dynamic listRepetitions;
  final SeriesModel series;
  @override
  ExerciseCircularIndicatorState createState() => ExerciseCircularIndicatorState(exercise, series, listRepetitions);
}

class ExerciseCircularIndicatorState extends State<ExerciseCircularIndicator> with TickerProviderStateMixin {
  Animation animation;
  AnimationController controller;

  Animation animationResting;
  AnimationController controllerResting;

  ExerciseModel exercise;
  SeriesModel series;
  dynamic listRepetitions;
  bool _isReady = false;
  bool _isStopped = false;
  bool _isFinished = false;
  bool _isResting = false;
  ExerciseCircularIndicatorState(this.exercise, this.series, this.listRepetitions);
  
  int completedSets;
  int completedReps = 0;

  void _LogReadyPressed() {
    if(!statusExists(seriesId: this.series.id, exerciseName: this.exercise.name, status: "series_start")) /* Has not yet logged this series has begun */ {
      logSeriesStart(this.series);
      ExercisesCompleteness.appendExerciseCompleteness(
        ExerciseCompletenessModel(
          series: this.series,
          exercise: this.exercise,
          completenessPercentage: null,
          timestamp: DateTime.now(),
          status: "series_start"
        )
      );
    }
    
    if(!statusExists(seriesId: this.series.id, exerciseName: this.exercise.name, status: "exercise_start")) /* Has not yet logged this series has begun */ {
      logExerciseStart(this.series, this.exercise);
      ExercisesCompleteness.appendExerciseCompleteness(
        ExerciseCompletenessModel(
          series: this.series,
          exercise: this.exercise,
          completenessPercentage: null,
          timestamp: DateTime.now(),
          status: "exercise_start"
        )
      );
    }
  }

  void _logSetFinished(int finishedSets, totalSets) {
    double completenessPercentage = num.parse((finishedSets / totalSets).toStringAsFixed(4)) * 100;
    logExerciseCompletenessPercentage(this.series, this.exercise, completenessPercentage);
    ExercisesCompleteness.appendExerciseCompleteness(
      ExerciseCompletenessModel(
        series: this.series,
        exercise: this.exercise,
        timestamp: DateTime.now(),
        status: "exercise_completeness_" + completenessPercentage.toString(),
        completenessPercentage: completenessPercentage
      )
    );
  }

  void _logExerciseFinished() {
    logExerciseEnd(this.series, this.exercise);
    ExercisesCompleteness.appendExerciseCompleteness(
      ExerciseCompletenessModel(
        series: this.series,
        exercise: this.exercise,
        timestamp: DateTime.now(),
        status: "exercise_end",
        completenessPercentage: null
      )
    );

    if(checkSeriesFinished(series: this.series)) {
      logSeriesEnd(this.series);
      ExercisesCompleteness.appendExerciseCompleteness(
        ExerciseCompletenessModel(
          series: this.series,
          exercise: null,
          timestamp: DateTime.now(),
          status: "series_end",
          completenessPercentage: null
        )
      );
      ExercisesCompleteness.appendCompletedSeries(this.series);
    }
  }

  int getCompletedSets() {
    List exerciseCompletenessLogs = ExercisesCompleteness.exerciseCompletenessList.where((ExerciseCompletenessModel exerciseCompleteness) {
      return exerciseCompleteness.series == this.series &&
      exerciseCompleteness.exercise == this.exercise &&
      exerciseCompleteness.completenessPercentage != null;
    }).toList();
    
    if(exerciseCompletenessLogs.length > 0){
      double highestPercentage = 0;
      for(ExerciseCompletenessModel exerciseCompleteness in exerciseCompletenessLogs) {
        double _percentage = exerciseCompleteness.completenessPercentage;
        if (_percentage > highestPercentage) highestPercentage = _percentage;
      } 
      return ((highestPercentage / 100) * listRepetitions["sets"]).round() + 1;
    }
    return 1;
  }

  @override
  void initState() {
    super.initState();
    completedSets = getCompletedSets();

    controller = AnimationController(
      duration: Duration(milliseconds: exercise.repetition_duration.round()), 
      reverseDuration:  Duration(milliseconds: exercise.repetition_reverse_duration.round()), 
      vsync: this
    );
    controllerResting = AnimationController(duration: const Duration(milliseconds: 30000), vsync: this);

    animationResting = CurvedAnimation(parent: controllerResting, curve: Curves.linear, reverseCurve: Curves.linear)
    ..addStatusListener((statusResting) {
      if (statusResting == AnimationStatus.completed) {
        setState(() {
          _isResting = false;
          controllerResting.reset();
          controller.forward();
        });
      } 
    });

    animation = CurvedAnimation(parent: controller, curve: Curves.easeInOutCubic, reverseCurve: Curves.linear)
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Future.delayed(Duration(milliseconds: 250), () {
            controller.reverse();
          });
        } else if (status == AnimationStatus.dismissed) {
          Future.delayed(Duration(milliseconds: 250), () {
            setState(() {
              if(_isReady && _isResting == false) {
                if(completedReps == listRepetitions["repetitions"]) {
                  if(completedSets == listRepetitions["sets"] && _isFinished == false)
                  {
                    _logExerciseFinished();
                    _isFinished = true;
                    completedSets++;
                    controller.stop(canceled: true);
                  }
                  else if(completedSets < listRepetitions["sets"]) {
                    _logSetFinished(completedSets, listRepetitions["sets"]);
                    completedReps = 0;
                    completedSets++;

                    controller.reset();
                    _isResting = true;
                    
                    controllerResting.forward();
                  }
                }
                else {
                  completedReps++;
                  controller.forward();
                }
              }
              else if(_isReady && _isResting) {
                controller.stop();
                controller = AnimationController(
                  duration: Duration(milliseconds: exercise.repetition_duration.round()), 
                  reverseDuration: Duration(milliseconds: exercise.repetition_reverse_duration.round()),
                  vsync: this
                );
                controller.forward();
              }
            });
          });
        }
      });
  }

  @override
  Widget build(BuildContext context) {

    UserModel model = User.instance;

    return  Scaffold(
      appBar: new AppBar(
        elevation: 1,
        backgroundColor: Colors.black87,
        title: Text(exercise.name),
        centerTitle: true,
      ), 
      body: Container(
        color: Colors.black87,
        child: Center (
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: Text(
                  exercise.difficulty != null ? "Sets: ${completedSets-1}/"+ listRepetitions["sets"].toString()+"\n" + 
                  Localization.of(context).translate("Exercise_Difficulty")+": ${exercise.difficulty[model.experience]} ${exercise.unit}"
                  :
                  "Sets: ${completedSets-1}/"+ listRepetitions["sets"].toString(),
                  textAlign: TextAlign.center,
                  
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 35
                  ),
                ),
                margin: EdgeInsets.only(bottom: 65),
              ),
              Container(
                height: 210,
                width: 210,
                child: _isReady == false && _isFinished == false ? RaisedButton(
                  color: Colors.deepOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(150.0),
                    side: BorderSide(
                      color: Colors.deepOrange
                    )
                  ),
                  onPressed: () {
                    setState(() {
                      _LogReadyPressed();
                      _isReady = !_isReady;
                      controller.forward();
                    });
                  },
                  child: Text(
                    Localization.of(context).translate("Exercise_Ready"),
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white
                    ),
                  ),
                ) 
                : 
                _isFinished == true ? RaisedButton(
                  color: Colors.deepOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(150.0),
                    side: BorderSide(
                      color: Colors.deepOrange
                    )
                  ),
                  onPressed: () {
                    setState(() {
                      Navigator.pop(context);
                    });
                  },
                  child: Text(
                    Localization.of(context).translate("Exercise_Finish"),
                    style: TextStyle(
                      fontSize: 30,
                      color: Colors.white
                    ),
                  ),
                ) 
                :
                _isResting == false ?
                CircleProgressBarContainer(completedReps, false, animation: animation)
                :
                CircleProgressBarContainer(completedReps, true, animation: animationResting),
              ),
              SizedBox(
                height: 35
              ),
              _isReady == true && _isFinished == false  && _isResting == false ? 
                Container( 
                  height: 90,
                  width: 90,
                  child: RaisedButton(
                  color: Colors.deepOrange,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(150.0),
                    side: BorderSide(
                      color: Colors.deepOrange
                    )
                  ),
                  onPressed: () {
                    setState(() {
                      _LogReadyPressed();
                      _isStopped = !_isStopped;
                      if(_isStopped)
                        controller.stop();
                      else {
                        if (animation.status == AnimationStatus.completed){
                          controller.reverse();
                        }
                        else if(animation.status == AnimationStatus.reverse){
                          controller.reverse();
                        }
                        else{
                          controller.forward();
                        }
                      }
                    });
                  },
                  child: Icon(Icons.pause, color: Colors.white,)
                )
              ) : SizedBox()
            ],
          )
        )
      )
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class CircleProgressBarContainer extends AnimatedWidget {
  static final _progressTween = Tween<double>(begin: 0.0, end: 1.0);
  final int reps;
  final bool rest;
  CircleProgressBarContainer(this.reps, this.rest, {Key key, Animation<double> animation})
  : super(key: key, listenable: animation);

  Widget build(BuildContext context) {
    final Animation<double> animation = listenable;
    return CircleProgressBar(
      foregroundColor: Colors.deepOrange,
      backgroundColor: Colors.black,
      value: _progressTween.evaluate(animation),
      reps: reps,
      rest: rest,
    );
  }
}

class CircleProgressBar extends StatelessWidget {
  final Color backgroundColor;
  final Color foregroundColor;
  final double value;
  final int reps;
  final bool rest;

  const CircleProgressBar({
    Key key,
    this.backgroundColor,
    @required this.foregroundColor,
    @required this.value,
    this.reps,
    this.rest
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final backgroundColor = this.backgroundColor;
    final foregroundColor = this.foregroundColor;
    return AspectRatio(
      aspectRatio: 1,
      child: CustomPaint(
        child: Container(
          child: Center(
            child: Text(
              rest == true ? Localization.of(context).translate("Exercise_Rest") : reps.toString(),
              style: TextStyle(
                color: Colors.white,
                fontSize: 35
              ),
            )
          ),
        ),
        foregroundPainter: CircleProgressBarPainter(
          backgroundColor: backgroundColor,
          foregroundColor: foregroundColor,
          percentage: this.value,
        ),
      ),
    );
  }
}

class CircleProgressBarPainter extends CustomPainter {
  final double percentage;
  final double strokeWidth;
  final Color backgroundColor;
  final Color foregroundColor;

  CircleProgressBarPainter({
    this.backgroundColor,
    @required this.foregroundColor,
    @required this.percentage,
    double strokeWidth,
  }) : this.strokeWidth = strokeWidth ?? 10;

  @override
  void paint(Canvas canvas, Size size) {
    final Offset center = size.center(Offset.zero);
    final Size constrainedSize = size - Offset(this.strokeWidth, this.strokeWidth);
    final shortestSide = Math.min(constrainedSize.width, constrainedSize.height);
    final foregroundPaint = Paint()
      ..color = this.foregroundColor
      ..strokeWidth = this.strokeWidth
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    final radius = (shortestSide / 2);

    // Start at the top. 0 radians represents the right edge
    final double startAngle = -(2 * Math.pi * 0.25);
    final double sweepAngle = (2 * Math.pi * (this.percentage ?? 0));

    // Don't draw the background if we don't have a background color
    if (this.backgroundColor != null) {
      final backgroundPaint = Paint()
        ..color = this.backgroundColor
        ..strokeWidth = this.strokeWidth
        ..style = PaintingStyle.stroke;
      canvas.drawCircle(center, radius, backgroundPaint);
    }

    canvas.drawArc(
      Rect.fromCircle(center: center, radius: radius),
      startAngle,
      sweepAngle,
      false,
      foregroundPaint,
    );
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    final oldPainter = (oldDelegate as CircleProgressBarPainter);
    return oldPainter.percentage != this.percentage ||
        oldPainter.backgroundColor != this.backgroundColor ||
        oldPainter.foregroundColor != this.foregroundColor ||
        oldPainter.strokeWidth != this.strokeWidth;
  }
}